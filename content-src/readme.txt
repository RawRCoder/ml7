﻿ -RU-

---ОПИСАНИЕ---

В данном архиве вы найдёте оригинальные исходники карт (vmf файлы) Half-Life 1, Counter Strike и Deathmatch Classic.

---ИНСТРУКЦИИ ПО УСТАНОВКЕ---

1) Извлечь из этого архива папку hl1-wc-maps в любое удобное для вас место

2) После распаковки запускаем редактор карт и открываем исходники.


Скачано с сайта http://www.hl2-beta.ru

---------------------------------------------------------------------------------------------------------------

 -ENG-

---DESCRIPTION---

You will find the original map sources (vmf files) of Half-Life 1, Counter Strike and Deathmatch Classic in this archive.


---INSTALLATION INSTRUCTIONS---

1) Unpack the hl1-wc-maps of this archive to any place you like on your PC.

2) After unpacking it, run the World Craft and open those maps.


Downloaded from http://www.hl2-beta.ru