#pragma once
#include "ml7_weapons_structs.h"


class AmmoTypesRepository
{
public:
	static const AmmoTypesRepository* Get();
	const AmmoType* FindById(AmmoTypeId id) const;
	const AmmoType* FindAmmoType(const char* name) const;
private:
	AmmoTypesRepository() = default;
	void Initialize();
	AmmoType* types[MAX_AMMO_TYPE_ID]{};
	AmmoType* DefineAmmoType(AmmoTypeId id, const char* name);
	static const AmmoTypesRepository* instance;
};