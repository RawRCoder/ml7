#pragma once
#include "ml7_weapons_structs.h"

class ShellTypesRepository
{
public:
	static const ShellTypesRepository* Get();
	const ShellType& GetShellTypeById(ShellTypeId id) const;
private:
	void Initialize();
	ShellType& DefineNewType(ShellTypeId id, const char* modelName, int bounceSound = TE_BOUNCE_SHELL);
	
	ShellType types[MAX_SHELL_TYPE_ID]{};
	static const ShellTypesRepository* instance;
};
