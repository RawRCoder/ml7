#include "ml7_weapons_structs.h"
#include "ammo_types.h"
#include "weapon_types.h"

const WeaponInfoRepository* WeaponInfoRepository::instance = nullptr;

const WeaponInfoRepository* WeaponInfoRepository::Get()
{
	if (!instance)
	{
		auto e = new WeaponInfoRepository();
		e->Initialize();
		instance = e;
	}
	return instance;
}

const WeaponInfo& WeaponInfoRepository::GetInfo(WeaponId id) const
{
	if (id < 0 || id >= MAX_WEAPON_ID)
		throw "unknown weapon id";
	return types[id];
}

const WeaponInfo* WeaponInfoRepository::FindInfo(WeaponId id) const
{
	if (id < 0 || id >= MAX_WEAPON_ID)
		return nullptr;
	return &types[id];
}

void UsePrimaryAmmo(WeaponInfo& info, AmmoTypeId ammoTypeId, int maxClip = -1)
{
	info.ammoTypeId = ammoTypeId;
	info.maxClip = maxClip;
}
void UseSecondaryAmmo(WeaponInfo& info, AmmoTypeId ammoTypeId, int maxClip = -1)
{
	info.secondaryAmmoTypeId = ammoTypeId;
	info.maxSecondaryClip = maxClip;
}

void PlaceInHud(WeaponInfo& info, int slot, int position)
{
	info.hudWeaponSlot = slot;
	info.hudWeaponSlotPosition = position;
}

void AddFlag(WeaponInfo& info, WeaponFlags flag)
{
	info.flags = static_cast<WeaponFlags>(info.flags | flag);
}

void WeaponInfoRepository::Initialize()
{
	// == HUD SLOT #0 - Melee ===============
	{
		auto& info = DefineNewType(Weapon_Crowbar, "weapon_crowbar");
		PlaceInHud(info, 0, 0);
		info.canFireUnderwater = true;
		info.autoselectWeight = 0;
		info.aiIsMelee = true;
	}
	
	// == HUD SLOT #1 - Pistols ==============
	{
		auto& info = DefineNewType(Weapon_Glock17, "weapon_9mmhandgun");
		PlaceInHud(info, 1, 0);
		UsePrimaryAmmo(info, Ammo_9x19mm, 17);
		info.canFireUnderwater = true;
		info.autoselectWeight = 10;
	}
	{
		auto& info = DefineNewType(Weapon_Usp45, "weapon_usp45");
		PlaceInHud(info, 1, 1);
		UsePrimaryAmmo(info, Ammo_45acp, 12);
		info.canFireUnderwater = true;
		info.autoselectWeight = 11;
	}
	{
		auto& info = DefineNewType(Weapon_Python, "weapon_357");
		PlaceInHud(info, 1, 2);
		UsePrimaryAmmo(info, Ammo_357Magnum, 6);
		info.autoselectWeight = 15;
	}

	// == HUD SLOT #2 - Primary ===============
	{
		auto& info = DefineNewType(Weapon_Mp5, "weapon_9mmAR");
		PlaceInHud(info, 2, 0);
		UsePrimaryAmmo(info, Ammo_9x19mm, 30);
		info.autoselectWeight = 15;
		info.npcAnimationSuffix = "mp5";
	}
	{
		auto& info = DefineNewType(Weapon_Shotgun, "weapon_shotgun");
		PlaceInHud(info, 2, 1);
		UsePrimaryAmmo(info, Ammo_00Buckshot, 8);
		info.autoselectWeight = 15;
		info.npcAnimationSuffix = "shotgun";
	}
	{
		auto& info = DefineNewType(Weapon_Crossbow, "weapon_crossbow");
		PlaceInHud(info, 2, 2);
		UsePrimaryAmmo(info, Ammo_CrossbowBolts, 5);
		info.canFireUnderwater = true;
		info.autoselectWeight = 10;
		info.npcAnimationSuffix = "shotgun";
	}
	{
		auto& info = DefineNewType(Weapon_M4A1, "weapon_m4a1");
		PlaceInHud(info, 2, 3);
		UsePrimaryAmmo(info, Ammo_556x45mm_NATO, 30);
		UseSecondaryAmmo(info, Ammo_40x46mm_M381);
		info.autoselectWeight = 16;
		info.npcAnimationSuffix = "m4";
	}

	// == HUD SLOT #3 - Heavy =================
	{
		auto& info = DefineNewType(Weapon_RocketLauncher, "weapon_rpg");
		PlaceInHud(info, 3, 0);
		UsePrimaryAmmo(info, Ammo_Rockets, 1);
		info.autoselectWeight = 19;
		info.npcAnimationSuffix = "rpg";
		info.aiMaxRange = 8192;
		info.aiMinSafeRange = 256; 
	}
	{
		auto& info = DefineNewType(Weapon_TauCannon, "weapon_gauss");
		PlaceInHud(info, 3, 1);
		UsePrimaryAmmo(info, Ammo_Uranium);
		info.autoselectWeight = 20;
		info.npcAnimationSuffix = "shotgun";
		info.aiMaxRange = 8192;
	}
	{
		auto& info = DefineNewType(Weapon_GluonGun, "weapon_egon");
		PlaceInHud(info, 3, 2);
		UsePrimaryAmmo(info, Ammo_Uranium);
		info.autoselectWeight = 20;
		info.npcAnimationSuffix = "shotgun";
		info.aiMaxRange = 4096;
	}
	{
		auto& info = DefineNewType(Weapon_HornetGun, "weapon_hornetgun");
		PlaceInHud(info, 3, 3);
		UsePrimaryAmmo(info, Ammo_Hornets);
		AddFlag(info, WeaponFlags_NoAutoSwitchEmpty);
		AddFlag(info, WeaponFlags_NoAutoReload);
		info.autoselectWeight = 10;
		info.npcAnimationSuffix = "mp5";
		info.aiMaxRange = 4096;
	}

	// == HUD SLOT #4 - Throwables ============
	{
		auto& info = DefineNewType(Weapon_HandGrenade, "weapon_handgrenade");
		PlaceInHud(info, 4, 0);
		UsePrimaryAmmo(info, Ammo_HandGrenades);
		AddFlag(info, WeaponFlags_LimitWorld);
		AddFlag(info, WeaponFlags_Exhaustible);
		info.canFireUnderwater = true;
		info.autoselectWeight = 5;
		info.aiMaxRange = 1024;
	}
	{
		auto& info = DefineNewType(Weapon_Satchel, "weapon_satchel");
		PlaceInHud(info, 4, 1);
		UsePrimaryAmmo(info, Ammo_Satchels);
		AddFlag(info, WeaponFlags_LimitWorld);
		AddFlag(info, WeaponFlags_Exhaustible);
		AddFlag(info, WeaponFlags_SelectOnEmpty);
		info.canFireUnderwater = true;
		info.autoselectWeight = -10;
		info.aiMaxRange = 1024;
	}
	{
		auto& info = DefineNewType(Weapon_TripMine, "weapon_tripmine");
		PlaceInHud(info, 4, 2);
		UsePrimaryAmmo(info, Ammo_TripMines);
		AddFlag(info, WeaponFlags_LimitWorld);
		AddFlag(info, WeaponFlags_Exhaustible);
		info.autoselectWeight = -10;
		info.aiMaxRange = 70;
	}
	{
		auto& info = DefineNewType(Weapon_Snarks, "weapon_snark");
		PlaceInHud(info, 4, 3);
		UsePrimaryAmmo(info, Ammo_Snarks);
		AddFlag(info, WeaponFlags_LimitWorld);
		AddFlag(info, WeaponFlags_Exhaustible);
		info.canFireUnderwater = true;
		info.autoselectWeight = 5;
		info.aiMaxRange = 512;
	}

	// == HUD SLOT #5 - Heavy 2 ===============
	{
		auto& info = DefineNewType(Weapon_M249, "weapon_m249");
		PlaceInHud(info, 5, 0);
		UsePrimaryAmmo(info, Ammo_556x45mm_NATO, 100);
		info.autoselectWeight = 22;
		info.npcAnimationSuffix = "m249";
	}
}

WeaponInfo& WeaponInfoRepository::DefineNewType(WeaponId id, const char* className)
{
	auto& item = types[id];
	item = WeaponInfo();
	item.id = id;
	item.className = className;
	return item;
}


const AmmoType* WeaponInfo::GetAmmoType() const
{
	if (ammoTypeId <= Ammo_None)
		return nullptr;

	const auto repo = AmmoTypesRepository::Get();
	return repo->FindById(ammoTypeId);
}

const AmmoType* WeaponInfo::GetSecondaryAmmoType() const
{
	if (secondaryAmmoTypeId <= Ammo_None)
		return nullptr;

	const auto repo = AmmoTypesRepository::Get();
	return repo->FindById(secondaryAmmoTypeId);
}
