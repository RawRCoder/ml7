#include	"extdll.h"
#include	"plane.h"
#include	"util.h"
#include	"cbase.h"
#include	"monsters.h"
#include	"schedule.h"
#include	"animation.h"
#include	"squadmonster.h"
#include	"weapons.h"
#include	"talkmonster.h"
#include	"soundent.h"
#include	"effects.h"
#include	"customentity.h"
#include    "game_monster_spawning.h"
#include "nodes.h"
#include "entities/xen_portal.h"


const spawnable_item* spawnable_item::all[] = {
	new spawnable_item{"monster_headcrab", 1.00f, SpawnCategory::Alien, SpawnEffects::XenPortal, head_hull},
	new spawnable_item{"monster_houndeye", .80f, SpawnCategory::Alien, SpawnEffects::XenPortal},
	new spawnable_item{"monster_bullchicken", .50f, SpawnCategory::Alien, SpawnEffects::XenPortal},
	new spawnable_item{"monster_alien_slave", .90f, SpawnCategory::Alien, SpawnEffects::XenPortal},
	new spawnable_item{"monster_alien_grunt", .30f, SpawnCategory::Alien, SpawnEffects::XenPortal, large_hull},
	new spawnable_item{"monster_alien_controller", .10f, SpawnCategory::Alien, SpawnEffects::XenPortal,
		human_hull, true, false, false	},
	new spawnable_item{"monster_gargantua", .03f, SpawnCategory::Alien, SpawnEffects::XenPortal, large_hull, false, true, false, 3},

	new spawnable_item{"monster_zombie", 0, SpawnCategory::Alien, SpawnEffects::XenPortal},
	
	new spawnable_item{"monster_apache", .01f, SpawnCategory::HumanMilitary, SpawnEffects::None, large_hull,
		true, false, false, 2},
	new spawnable_item{"monster_barney", .25f, SpawnCategory::HumanMilitary},
	new spawnable_item{"monster_human_grunt", .50f, SpawnCategory::HumanMilitary},
	new spawnable_item{"monster_human_assassin", .50f, SpawnCategory::BlackOps, SpawnEffects::None}
};
const int spawnable_item::count = sizeof(spawnable_item::all) / sizeof(spawnable_item*);
int spawnable_item::g_iRecursionDepth = 0;
int spawnable_item::g_bShouldSpawnWithEffects = -1;

bool Chance(float chance) { return RANDOM_FLOAT(0, 1) < chance; }

Vector RandomVector()
{
	while (1)
	{
		const auto a = RANDOM_FLOAT(-1, +1);
		const auto b = RANDOM_FLOAT(-1, +1);
		const auto kk = a * a + b * b;
		if (kk >= 1) continue;

		const auto k2 = sqrtf(1 - kk);
		return {2 * a * k2, 2 * b * k2, 1 - 2 * kk};
	}
}

const bool CheckMaxCount(const spawnable_item* spawnable)
{
	if (spawnable->maxCount < 0)
		return true;
	if (spawnable->maxCount == 0)
		return false;

	auto iCount = 0;
	CBaseEntity * pEntity = nullptr;
	while (pEntity = UTIL_FindEntityByClassname(pEntity, spawnable->className))
	{
		if (!pEntity->IsAlive())
			continue;

		iCount++;
		if (iCount >= spawnable->maxCount)
			return false;
	}
	return true;
}

const spawnable_item* ChooseOne(const SpawnCategory category)
{
	const spawnable_item* items[spawnable_item::count];
	auto itemsCount = 0;
	auto sum = 0.f;

	for (auto& item : spawnable_item::all)
	{
		if (item->type != category || item->weight <= 0)
			continue;
		sum += item->weight;
		items[itemsCount++] = item;
	}

	if (itemsCount < 1 || sum <= 0)
		return nullptr;

	auto rnd = RANDOM_FLOAT(0, sum);
	for (auto i = 0; i < itemsCount; ++i)
	{
		const auto item = items[i];
		rnd -= item->weight;
		if (rnd <= 0 && CheckMaxCount(item))
			return item;
	}
	return nullptr;
}


bool IsPointSuitable(const Vector& p, int flags)
{
	switch(UTIL_PointContents(p))
	{
	case CONTENTS_EMPTY:
		return flags & (bits_NODE_AIR | bits_NODE_LAND);
	case CONTENTS_WATER:
		return flags & bits_NODE_WATER;
	}
	return false;
}

bool FindPlaceNear(const Vector& source, Vector& result, bool extendedRange, int flags, int hullType)
{
	const auto flAngle = RANDOM_FLOAT(0, 3.1415926535897931f * 2);
	const auto flDist = extendedRange ? RANDOM_FLOAT(0, 8192) : RANDOM_FLOAT(0, 1280);
	auto posToCheck = source + Vector(cosf(flAngle), sinf(flAngle), 0) * flDist;

	TraceResult tr;
	UTIL_TraceHull(source, posToCheck, dont_ignore_monsters, human_hull, nullptr,
	               &tr);
	if (tr.fAllSolid)
		return false;

	if (tr.flFraction >= 1)
	{
		if (IsPointSuitable(posToCheck, flags))
		{
			result = posToCheck;
			return true;
		}
	}
	if (tr.flFraction > 0)
	{
		UTIL_TraceHull(tr.vecEndPos, tr.vecEndPos + tr.vecPlaneNormal * 64, dont_ignore_monsters, human_hull,
		               nullptr, &tr);
		if (tr.flFraction >= 1)
		{
			posToCheck = tr.vecEndPos + tr.vecPlaneNormal * 64;
			if (IsPointSuitable(posToCheck, flags))
			{
				result = posToCheck;
				return true;
			}
		}
	}
	return false;
}

bool FindPlaceForAMonster(Vector& origin, const spawnable_item* spawnable)
{
	auto targetKindFlags = (spawnable->canFly ? bits_NODE_AIR : 0)
		| (spawnable->canSwim ? bits_NODE_WATER : 0)
		| (spawnable->canWalk ? bits_NODE_LAND : 0);
	for (auto attemptID = 0; attemptID < 10; ++attemptID)
	{
		const auto vecDirection = RandomVector() * RANDOM_LONG(0, 4096);

		const auto iNode = WorldGraph.FindNearestNode(origin + vecDirection, targetKindFlags);
		if (iNode != NO_NODE && RANDOM_FLOAT(0, 1) < 0.3f)
		{
			auto& node = WorldGraph.Node(iNode);
			if (FindPlaceNear(node.m_vecOrigin, origin, false, targetKindFlags, spawnable->hullType))
				return true;
		}
		else
		{
			if (FindPlaceNear(origin, origin, true, targetKindFlags, spawnable->hullType))
				return true;
		}
	}
	return false;
}

CBaseEntity* spawnable_item::SpawnOne(const SpawnCategory category, const Vector& origin, const bool noEffects)
{
	if (g_iRecursionDepth > 1)
		return nullptr;

	if (g_bShouldSpawnWithEffects < 0)
		g_bShouldSpawnWithEffects = !noEffects;

	auto actualOrigin = origin;
	const auto pSpawnable = ChooseOne(category);
	if (!pSpawnable)
		return nullptr;
	const Vector entAngles = {0, RANDOM_FLOAT(0, 360), 0};

	if (!FindPlaceForAMonster(actualOrigin, pSpawnable))
		return nullptr;

	if (noEffects || pSpawnable->effects == SpawnEffects::None)
	{
		g_iRecursionDepth++;
		const auto pEntity = CBaseEntity::Create((char*)pSpawnable->className, actualOrigin, entAngles);
		if (pEntity) { pEntity->pev->spawnflags |= SF_MONSTER_FALL_TO_GROUND | SF_MONSTER_FADECORPSE; }
		--g_iRecursionDepth;

		return pEntity;
	}

	return CXenPortal::MakePortal(pSpawnable->className, actualOrigin);
}

CBaseEntity* spawnable_item::SpawnOne(const SpawnCategory category, const Vector& origin)
{
	ALERT(at_notice, "SpawnOne with recursion %i\n", g_iRecursionDepth);
	return SpawnOne(category, origin, spawnable_item::g_bShouldSpawnWithEffects == 0);
}
