#pragma once
#include "ml7_weapons_structs.h"

class WeaponInfoRepository
{
public:
	static const WeaponInfoRepository* Get();
	const WeaponInfo& GetInfo(WeaponId id) const;
	const WeaponInfo* FindInfo(WeaponId id) const;
private:
	void Initialize();
	WeaponInfo& DefineNewType(WeaponId id, const char* className);
	
	WeaponInfo types[MAX_WEAPON_ID] { };
	static const WeaponInfoRepository* instance;
};
