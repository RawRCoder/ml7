#include "extdll.h"
#include "util.h"
#include "cbase.h"
#include "weapons.h"

const AmmoTypesRepository* AmmoTypesRepository::instance = nullptr;


float AmmoType::GetShotEnergy() const
{
	return bulletMass * bulletVelocity* bulletVelocity / 2;
}

const AmmoTypesRepository* AmmoTypesRepository::Get()
{
	if (!instance)
	{
		auto e = new AmmoTypesRepository();
		e->Initialize();
		instance = e;
	}
	return instance;
}

void AmmoTypesRepository::Initialize()
{
	{
		auto ammo = DefineAmmoType(Ammo_00Buckshot, "buckshot");
		ammo->maxCarry = 8 * 5;

		ammo->bulletType = BULLET_BUCKSHOT;
		ammo->bulletCaliber = 8.38f * 0.001; //8.38mm
		ammo->bulletMass = 3.486181 * 0.001;  //3.486g
		ammo->bulletCount = 9;
		ammo->bulletVelocity = 403.86f;
		ammo->bulletMaxDistance = 10*ammo->bulletVelocity / ammo->armorPiercingK;
		ammo->shellTypeId = Shell_Buckshot;
	}
	{
		auto ammo = DefineAmmoType(Ammo_9x19mm, "9mm");
		ammo->maxCarry = 30 * 6;

		ammo->bulletType = BULLET_9MM_PARABELLUM;
		ammo->bulletCaliber = 9 * 0.001;
		ammo->bulletMass = 8.04f * 0.001;  //124-grain bullet	
		ammo->bulletVelocity = 350.0f;
		ammo->armorPiercingK *= 2.00f;
		ammo->bulletMaxDistance = 10 * ammo->bulletVelocity / ammo->armorPiercingK;
		ammo->shellTypeId = Shell_9x19mm;
	}
	{
		auto ammo = DefineAmmoType(Ammo_45acp, ".45ACP");
		ammo->maxCarry = 30 * 5;

		ammo->bulletType = BULLET_9MM_PARABELLUM;
		ammo->bulletCaliber = 11.43 * .001f;
		ammo->bulletMass = 15.f * .001f;  
		ammo->bulletVelocity = 255.f;
		ammo->armorPiercingK *= 1.00f;
		ammo->bulletMaxDistance = 10 * ammo->bulletVelocity / ammo->armorPiercingK;
		ammo->shellTypeId = Shell_45acp;
	}
	{
		auto ammo = DefineAmmoType(Ammo_40x46mm_M381, "ARgrenades");
		ammo->maxCarry = 1 * 5;
		ammo->bulletCaliber = 40 * 0.001f;
		ammo->bulletMass = 0.23f;
		ammo->bulletVelocity = 76.2f;
	}
	{
		auto ammo = DefineAmmoType(Ammo_357Magnum, "357");
		ammo->maxCarry = 6 * 6;

		ammo->bulletType = BULLET_357_MAGNUM;
		ammo->bulletCaliber = 9.07 * 0.001f;
		ammo->bulletMass = 8.1f * 0.001f;  	
		ammo->bulletVelocity = 440;
		ammo->armorPiercingK *= 1.50f;
		ammo->bulletMaxDistance = 10 * ammo->bulletVelocity / ammo->armorPiercingK;
		ammo->shellTypeId = Shell_357Magnum;
	}
	{
		auto ammo = DefineAmmoType(Ammo_Uranium, "uranium");
		ammo->maxCarry = 100;		
	}
	{
		auto ammo = DefineAmmoType(Ammo_Rockets, "rockets");
		ammo->maxCarry = 5;
	}
	{
		auto ammo = DefineAmmoType(Ammo_CrossbowBolts, "bolts");
		ammo->maxCarry = 5*5;
	}
	{
		auto ammo = DefineAmmoType(Ammo_Satchels, "Satchel Charge");
		ammo->maxCarry = 5;
	}
	{
		auto ammo = DefineAmmoType(Ammo_TripMines, "Trip Mine");
		ammo->maxCarry = 5;
	}
	{
		auto ammo = DefineAmmoType(Ammo_HandGrenades, "Hand Grenade");
		ammo->maxCarry = 5;
	}
	{
		auto ammo = DefineAmmoType(Ammo_Snarks, "Snarks");
		ammo->maxCarry = 15;
	}
	{
		auto ammo = DefineAmmoType(Ammo_Hornets, "Hornets");
		ammo->maxCarry = 8;
	}


	{
		//SS109 FMJBT 
		auto ammo = DefineAmmoType(Ammo_556x45mm_NATO, "5.56x45mm NATO"); 
		ammo->maxCarry = 30 * 8;
		ammo->bulletCaliber = 5.56 * 0.001f;
		ammo->bulletVelocity = 948;
		ammo->bulletMass = 4.0f * 0.001f;
		ammo->armorPiercingK *= 4.00f;
		ammo->tracerFrequency = 4;
		ammo->bulletMaxDistance = 10 * ammo->bulletVelocity / ammo->armorPiercingK ;
		ammo->shellTypeId = Shell_556x45mm;
	}
	{
		auto ammo = DefineAmmoType(Ammo_762x51mm_NATO, "7.62x51mm NATO");
		ammo->maxCarry = 30 * 5;
		ammo->bulletCaliber = 7.62 * 0.001f;
		ammo->bulletVelocity = 860;
		ammo->bulletMass = 10.0f * 0.001f;
		ammo->armorPiercingK *= 4.00f;
		ammo->tracerFrequency = 4;
		ammo->bulletMaxDistance = 10 * ammo->bulletVelocity / ammo->armorPiercingK;
	}
	{
		auto ammo = DefineAmmoType(Ammo_50BMG, ".50 BMG");
		ammo->maxCarry = 100;
		ammo->bulletCaliber = 12.7f * 0.001f;
		ammo->bulletVelocity = 928;
		ammo->bulletMass = 42.0f * 0.001f;
		ammo->armorPiercingK *= 4.00f;
		ammo->tracerFrequency = 4;
		ammo->bulletMaxDistance = 10 * ammo->bulletVelocity / ammo->armorPiercingK;
	}
	{
		auto ammo = DefineAmmoType(Ammo_30x113mm, "30x113mm");
		ammo->maxCarry = 1500;
		ammo->bulletCaliber = 30.0f * 0.001f;
		ammo->bulletVelocity = 805;
		ammo->bulletMass = 229.0f * 0.001f;
		ammo->armorPiercingK *= 2.00f;
		ammo->tracerFrequency = 2;
		ammo->bulletMaxDistance = 10 * ammo->bulletVelocity / ammo->armorPiercingK;
		ammo->explosivesCharge = 0.022f;
	}
}


const AmmoType* AmmoTypesRepository::FindById(AmmoTypeId id) const
{
	if (id < 0 || id >= MAX_AMMO_TYPE_ID)
		return nullptr;

	const auto type = types[id];
	if (!type)
		return nullptr;

	return type;
}

const AmmoType* AmmoTypesRepository::FindAmmoType(const char* name) const
{
	for (auto ammoType : types)
	{
		if (!ammoType)
			continue;
		
		if (!strcmp(ammoType->name, name))
			return ammoType;
	}
	return nullptr;
}

AmmoType* AmmoTypesRepository::DefineAmmoType(const AmmoTypeId id, const char* name)
{
	auto item = types[id];
	delete item;


	item = new AmmoType();
	item->id = id;
	item->name = name;
	types[id] = item;
	return item;
}


