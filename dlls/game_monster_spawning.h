#pragma once

enum class SpawnCategory
{
	Generic,
	Alien,
	HumanMilitary,
	BlackOps,
	XRaceAlien
};

enum class SpawnEffects
{
	None,
	XenPortal
};

struct spawnable_item
{
public:
	const char* className = nullptr;
	const float weight = 0;
	const SpawnCategory type = SpawnCategory::Generic;
	const SpawnEffects effects = SpawnEffects::None;
	const hull_type hullType = human_hull;
	const bool canFly = false;
	const bool canWalk = true;
	const bool canSwim = false;
	const int maxCount = -1;


	static const spawnable_item* all[];
	static const int count;
	static int g_iRecursionDepth;
	static int g_bShouldSpawnWithEffects;

	static CBaseEntity* SpawnOne(SpawnCategory category, const Vector& origin, bool noEffects);
	static CBaseEntity* SpawnOne(SpawnCategory category, const Vector& origin);
private:
};

