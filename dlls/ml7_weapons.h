#pragma once
#include "ml7_weapons_structs.h"
#include "cbase.h"
#include "eiface.h"
#include "util.h"


class CBaseWeapon : public CBaseAnimating
{
public:
	virtual const char* GetName() const { return STRING(pev->classname); }
	void SetObjectCollisionBox() override;
	
	int		Save(CSave& save) override;
	int		Restore(CRestore& restore) override;
	static	TYPEDESCRIPTION m_SaveData[];

	virtual bool AddToUser(CBaseMonster* user);
	virtual bool AddDuplicate(CBaseWeapon* weapon);
	void Spawn() override;

	virtual WeaponId GetWeaponTypeId() const = 0;
	const WeaponInfo& GetWeaponInfo() const;
};
