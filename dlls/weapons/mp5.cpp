/***
*
*	Copyright (c) 1996-2001, Valve LLC. All rights reserved.
*	
*	This product contains software technology licensed from Id 
*	Software, Inc. ("Id Technology").  Id Technology (c) 1996 Id Software, Inc. 
*	All Rights Reserved.
*
*   Use, distribution, and modification of this source code and/or resulting
*   object code is restricted to non-commercial enhancements to products from
*   Valve LLC.  All other use, distribution, or modification is prohibited
*   without written permission from Valve LLC.
*
****/

#include "extdll.h"
#include "util.h"
#include "cbase.h"
#include "monsters.h"
#include "weapons.h"
#include "nodes.h"
#include "player.h"
#include "soundent.h"
#include "gamerules.h"

enum mp5_e
{
	MP5_LONGIDLE = 0,
	MP5_IDLE1,
	MP5_LAUNCH,
	MP5_RELOAD,
	MP5_DEPLOY,
	MP5_FIRE1,
	MP5_FIRE2,
	MP5_FIRE3,
};



LINK_ENTITY_TO_CLASS( weapon_mp5, CMP5 );
LINK_ENTITY_TO_CLASS( weapon_9mmAR, CMP5 );


void CMP5::Spawn( )
{
	pev->classname = MAKE_STRING("weapon_9mmAR"); // hack to allow for old names
	Precache( );
	SET_MODEL(ENT(pev), "models/weapons/w_mp5.mdl");
	m_iId = WEAPON_MP5;

	m_iDefaultAmmo = MP5_DEFAULT_GIVE;

	FallInit();// get ready to fall down.
}

const char* szMp5PrimaryFireSounds[] = {
	"weapons/hks1.wav",
	"weapons/hks2.wav",
	"weapons/hks3.wav"
};

const char* CMP5::GetPrimaryFireSound() const
{
	return szMp5PrimaryFireSounds[RANDOM_LONG(0, ARRAYSIZE(szMp5PrimaryFireSounds)-1)];
}

void CMP5::Precache( void )
{
	CBasePlayerWeapon::Precache();
	
	PRECACHE_MODEL("models/weapons/v_mp5.mdl");
	PRECACHE_MODEL("models/weapons/w_mp5.mdl");
	PRECACHE_MODEL("models/weapons/p_mp5.mdl");

	UTIL_PrecacheOther("ammo_mp5clip");
	PRECACHE_SOUND("items/9mmclip1.wav");              

	PRECACHE_SOUND("items/clipinsert1.wav");
	PRECACHE_SOUND("items/cliprelease1.wav");
	
	for (auto& szPrimaryFireSound : szMp5PrimaryFireSounds)
	{
		PRECACHE_SOUND(szPrimaryFireSound);
	}

	PRECACHE_SOUND ("weapons/357_cock1.wav");

	m_usMP5 = PRECACHE_EVENT( 1, "events/mp5.sc" );
}


bool CMP5::AddToPlayer(CBasePlayer* pPlayer)
{
	if ( CBasePlayerWeapon::AddToPlayer( pPlayer ) )
	{
		MESSAGE_BEGIN( MSG_ONE, gmsgWeapPickup, NULL, pPlayer->pev );
			WRITE_BYTE( m_iId );
		MESSAGE_END();
		return TRUE;
	}
	return FALSE;
}

BOOL CMP5::Deploy( )
{
	return DefaultDeploy( "models/weapons/v_mp5.mdl", "models/weapons/p_mp5.mdl", MP5_DEPLOY, "mp5" );
}


bool CMP5::PrimaryAttack()
{
	Vector vecDir;
	if (!DefaultPrimaryFire(vecDir, GetSpread(4 / 60.0f), 800, AUTOAIM_5DEGREES,
	                        2, -1.5f, .5f, .25f))
		return false;

	if (m_pPlayer)
	{
#if defined( CLIENT_WEAPONS )
		int flags = FEV_NOTHOST;
#else
		int flags = 0;
#endif

		PLAYBACK_EVENT_FULL(flags, m_pOperator->edict(), m_usMP5, 0.0, (float*)& g_vecZero, (float*)& g_vecZero, vecDir.x, vecDir.y, 0, 0, 0, 0);
	}
	return true;
}



void CMP5::Reload( void )
{
	if (GetPrimaryAmmoLeft() <= 0 )
		return;

	DefaultReload(MP5_RELOAD, 1.5);
}


void CMP5::WeaponIdle( void )
{
	ResetEmptySound( );

	Aim(AUTOAIM_5DEGREES);

	if ( m_flTimeWeaponIdle > WeaponTimeBase() )
		return;

	int iAnim;
	switch ( RANDOM_LONG( 0, 1 ) )
	{
	case 0:	
		iAnim = MP5_LONGIDLE;	
		break;
	
	default:
	case 1:
		iAnim = MP5_IDLE1;
		break;
	}

	SendWeaponAnim( iAnim );

	m_flTimeWeaponIdle = SharedRandom( 10, 15 ); // how long till we do this again.
}
