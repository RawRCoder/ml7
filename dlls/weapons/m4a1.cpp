/***
*
*	Copyright (c) 1996-2001, Valve LLC. All rights reserved.
*	
*	This product contains software technology licensed from Id 
*	Software, Inc. ("Id Technology").  Id Technology (c) 1996 Id Software, Inc. 
*	All Rights Reserved.
*
*   Use, distribution, and modification of this source code and/or resulting
*   object code is restricted to non-commercial enhancements to products from
*   Valve LLC.  All other use, distribution, or modification is prohibited
*   without written permission from Valve LLC.
*
****/

#include "extdll.h"
#include "util.h"
#include "cbase.h"
#include "monsters.h"
#include "weapons.h"
#include "nodes.h"
#include "player.h"
#include "soundent.h"
#include "gamerules.h"

enum m4a1_e
{
	M4A1_LONGIDLE = 0,
	M4A1_IDLE1,
	M4A1_LAUNCH,
	M4A1_RELOAD,
	M4A1_DEPLOY,
	M4A1_FIRE1,
	M4A1_FIRE2,
	M4A1_FIRE3,
};



LINK_ENTITY_TO_CLASS( weapon_m4a1, CM4A1 );



void CM4A1::Spawn( )
{
	pev->classname = MAKE_STRING("weapon_m4a1");
	Precache( );
	SET_MODEL(ENT(pev), "models/weapons/w_m4a1.mdl");
	m_iId = GetWeaponId();
	m_iDefaultAmmo = GetMaxClip();

	FallInit();// get ready to fall down.
}

const char* szM4PrimaryFireSounds[] = {
	"weapons/m4a1-1.wav",
	"weapons/m4a1-2.wav",
	"weapons/m4a1-3.wav"
};

const char* CM4A1::GetPrimaryFireSound() const
{
	return szM4PrimaryFireSounds[RANDOM_LONG(0, ARRAYSIZE(szM4PrimaryFireSounds)-1)];
}


void CM4A1::Precache()
{
	CBasePlayerWeapon::Precache();

	UTIL_PrecacheOther("ammo_m4a1_clip");

	PRECACHE_MODEL("models/weapons/v_m4a1.mdl");
	PRECACHE_MODEL("models/weapons/w_m4a1.mdl");
	PRECACHE_MODEL("models/weapons/p_m4a1.mdl");

	PRECACHE_MODEL("models/grenade.mdl");	// grenade
	
	PRECACHE_SOUND("items/9mmclip1.wav");              

	PRECACHE_SOUND("items/clipinsert1.wav");
	PRECACHE_SOUND("items/cliprelease1.wav");

	for (auto& szPrimaryFireSound : szM4PrimaryFireSounds)
	{
		PRECACHE_SOUND(szPrimaryFireSound);
	}

	PRECACHE_SOUND( "weapons/glauncher.wav" );
	PRECACHE_SOUND( "weapons/glauncher2.wav" );

	PRECACHE_SOUND ("weapons/357_cock1.wav");

	m_usM4A1 = PRECACHE_EVENT( 1, "events/m4a1.sc" );
	m_usM203 = PRECACHE_EVENT( 1, "events/m203.sc" );
}


bool CM4A1::AddToPlayer(CBasePlayer* pPlayer)
{
	if ( CBasePlayerWeapon::AddToPlayer( pPlayer ) )
	{
		MESSAGE_BEGIN( MSG_ONE, gmsgWeapPickup, NULL, pPlayer->pev );
			WRITE_BYTE( m_iId );
		MESSAGE_END();
		return TRUE;
	}
	return FALSE;
}

BOOL CM4A1::Deploy( )
{
	return DefaultDeploy( "models/weapons/v_m4a1.mdl", "models/weapons/p_m4a1.mdl", M4A1_DEPLOY, "mp5", 0);
}


bool CM4A1::PrimaryAttack()
{
	Vector vecDir;
	if (!DefaultPrimaryFire(vecDir, GetSpread(4 / 60.0f), 800, AUTOAIM_5DEGREES, 2, -2.f, .6f, .8f))
		return false;
	
	if (m_pPlayer)
	{
#if defined( CLIENT_WEAPONS )
		int flags = FEV_NOTHOST;
#else
		int flags = 0;
#endif

		PLAYBACK_EVENT_FULL(flags, m_pOperator->edict(), m_usM4A1, 0.0, (float*)& g_vecZero, (float*)& g_vecZero, vecDir.x, vecDir.y, 0, 0, 0, 0);
	}
	return true;
}



void CM4A1::SecondaryAttack( void )
{
	// don't fire underwater
	if (m_pOperator->pev->waterlevel == 3)
	{
		PlayEmptySound( );
		m_flNextPrimaryAttack = 0.15;
		return;
	}

	int& ammoLeft = GetSecondaryAmmoLeft();
	if (ammoLeft <= 0)
	{
		PlayEmptySound( );
		return;
	}

	if (m_pPlayer)
	{		
		m_pPlayer->m_iWeaponVolume = NORMAL_GUN_VOLUME;
		m_pPlayer->m_iWeaponFlash = BRIGHT_GUN_FLASH;

		m_pPlayer->m_iExtraSoundTypes = bits_SOUND_DANGER;
		m_pPlayer->m_flStopExtraSoundTime = WeaponTimeBase() + .2f;
		
		// player "shoot" animation
		m_pPlayer->SetAnimation(PLAYER_ATTACK1);
	}
			
	--ammoLeft;

 	UTIL_MakeVectors( m_pOperator->pev->v_angle + m_pOperator->pev->punchangle );

	// we don't add in player velocity anymore.
	CGrenade::ShootContact( m_pOperator->pev, 
							m_pOperator->pev->origin + m_pOperator->pev->view_ofs + gpGlobals->v_forward * 16, 
							gpGlobals->v_forward * 800 );

	int flags;
#if defined( CLIENT_WEAPONS )
	flags = FEV_NOTHOST;
#else
	flags = 0;
#endif

	PLAYBACK_EVENT( flags, m_pOperator->edict(), m_usM203 );
	
	m_flNextPrimaryAttack = GetNextAttackDelay(1);
	m_flNextSecondaryAttack = WeaponTimeBase() + 1;
	m_flTimeWeaponIdle = WeaponTimeBase() + 5;// idle pretty soon after shooting.

	if (ammoLeft <= 0)
		m_pOperator->OnOutOfAmmo(this);
}

void CM4A1::Reload( void )
{
	if (GetPrimaryAmmoLeft() <= 0 )
		return;

	DefaultReload( M4A1_RELOAD, 1.5);
}


void CM4A1::WeaponIdle( void )
{
	ResetEmptySound( );

	Aim(AUTOAIM_5DEGREES);

	if ( m_flTimeWeaponIdle > WeaponTimeBase() )
		return;

	int iAnim;
	switch ( RANDOM_LONG( 0, 1 ) )
	{
	case 0:	
		iAnim = M4A1_LONGIDLE;	
		break;
	
	default:
	case 1:
		iAnim = M4A1_IDLE1;
		break;
	}

	SendWeaponAnim( iAnim );

	m_flTimeWeaponIdle = SharedRandom(10, 15); // how long till we do this again.
}