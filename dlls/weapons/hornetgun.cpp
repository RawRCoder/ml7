/***
*
*	Copyright (c) 1996-2001, Valve LLC. All rights reserved.
*	
*	This product contains software technology licensed from Id 
*	Software, Inc. ("Id Technology").  Id Technology (c) 1996 Id Software, Inc. 
*	All Rights Reserved.
*
*   Use, distribution, and modification of this source code and/or resulting
*   object code is restricted to non-commercial enhancements to products from
*   Valve LLC.  All other use, distribution, or modification is prohibited
*   without written permission from Valve LLC.
*
****/
#if !defined( OEM_BUILD ) && !defined( HLDEMO_BUILD )

#include "extdll.h"
#include "util.h"
#include "cbase.h"
#include "monsters.h"
#include "weapons.h"
#include "nodes.h"
#include "player.h"
#include "monsters/hornet.h"
#include "gamerules.h"


enum hgun_e
{
	HGUN_IDLE1 = 0,
	HGUN_FIDGETSWAY,
	HGUN_FIDGETSHAKE,
	HGUN_DOWN,
	HGUN_UP,
	HGUN_SHOOT
};

enum firemode_e
{
	FIREMODE_TRACK = 0,
	FIREMODE_FAST
};


LINK_ENTITY_TO_CLASS(weapon_hornetgun, CHgun);

BOOL CHgun::IsUseable(void) { return TRUE; }

void CHgun::Spawn()
{
	Precache();
	m_iId = WEAPON_HORNETGUN;
	SET_MODEL(ENT(pev), "models/w_hgun.mdl");

	m_iDefaultAmmo = HIVEHAND_DEFAULT_GIVE;
	m_iFirePhase = 0;

	FallInit(); // get ready to fall down.
}


void CHgun::Precache(void)
{
	CBasePlayerWeapon::Precache();

	PRECACHE_MODEL("models/v_hgun.mdl");
	PRECACHE_MODEL("models/w_hgun.mdl");
	PRECACHE_MODEL("models/p_hgun.mdl");

	m_usHornetFire = PRECACHE_EVENT(1, "events/firehornet.sc");

	UTIL_PrecacheOther("hornet");
}

bool CHgun::AddToPlayer(CBasePlayer* pPlayer)
{
	if (!CBasePlayerWeapon::AddToPlayer(pPlayer))
		return false;
	
#ifndef CLIENT_DLL
	if (g_pGameRules->IsMultiplayer())
	{
		// in multiplayer, all hivehands come full. 
		pPlayer->m_rgAmmo[PrimaryAmmoIndex()] = FindPrimaryAmmoType()->maxCarry;
	}
#endif

	MESSAGE_BEGIN(MSG_ONE, gmsgWeapPickup, nullptr, pPlayer->pev);
	WRITE_BYTE(m_iId);
	MESSAGE_END();
	return true;
}

BOOL CHgun::Deploy() { return DefaultDeploy("models/v_hgun.mdl", "models/p_hgun.mdl", HGUN_UP, "hive"); }

void CHgun::Holster(int skiplocal /* = 0 */)
{
	m_pOperator->m_flNextAttack = WeaponTimeBase() + .5f;
	SendWeaponAnim(HGUN_DOWN);

	//!!!HACKHACK - can't select hornetgun if it's empty! no way to get ammo for it, either.
	if (m_pPlayer && !m_pPlayer->m_rgAmmo[PrimaryAmmoIndex()])
	{ m_pPlayer->m_rgAmmo[PrimaryAmmoIndex()] = 1; }
}


bool CHgun::PrimaryAttack()
{
	Reload();

	if (GetPrimaryAmmoLeft() <= 0)
		return false;

#ifndef CLIENT_DLL
	auto vecDir = Aim();

	auto pHornet = Create( "hornet",
		m_pOperator->GetGunPosition() + gpGlobals->v_forward * 16 + gpGlobals->v_right * 8 + gpGlobals->v_up * -12,
		m_pOperator->pev->v_angle, m_pOperator->edict());
	pHornet->pev->velocity = gpGlobals->v_forward * 300;

	m_flRechargeTime = gpGlobals->time + RANDOM_FLOAT(0, .75f);
#endif

	GetPrimaryAmmoLeft()--;


	if (m_pPlayer)
	{
		m_pPlayer->m_iWeaponVolume = QUIET_GUN_VOLUME;
		m_pPlayer->m_iWeaponFlash = DIM_GUN_FLASH;

		int flags;
#if defined( CLIENT_WEAPONS )
		flags = FEV_NOTHOST;
#else
		flags = 0;
#endif

		PLAYBACK_EVENT_FULL(flags, m_pPlayer->edict(), m_usHornetFire, 0.0, (float*)&g_vecZero, (float*)&g_vecZero, 0.0,
		                    0.0, FIREMODE_TRACK, 0, 0, 0);

		// player "shoot" animation
		m_pPlayer->SetAnimation(PLAYER_ATTACK1);
	}

	m_flNextPrimaryAttack = GetNextAttackDelay(SharedRandom(1.f / 10, 1.f / 4));

	if (m_flNextPrimaryAttack < WeaponTimeBase())
		m_flNextPrimaryAttack = WeaponTimeBase() + SharedRandom(1.f/10, 1.f/4);

	m_flTimeWeaponIdle = WeaponTimeBase() + SharedRandom(10, 15);
	return true;
}


void CHgun::SecondaryAttack(void)
{
	Reload();

	if (GetPrimaryAmmoLeft() <= 0) { return; }

	//Wouldn't be a bad idea to completely predict these, since they fly so fast...
#ifndef CLIENT_DLL

	UTIL_MakeVectors(m_pOperator->pev->v_angle);

	Vector vecSrc = m_pOperator->GetGunPosition()
		+ gpGlobals->v_forward * 16
		+ gpGlobals->v_right * 8
		+ gpGlobals->v_up * -12;

	m_iFirePhase++;
	switch (m_iFirePhase)
	{
	case 1:
		vecSrc = vecSrc + gpGlobals->v_up * 8;
		break;
	case 2:
		vecSrc = vecSrc + gpGlobals->v_up * 8;
		vecSrc = vecSrc + gpGlobals->v_right * 8;
		break;
	case 3:
		vecSrc = vecSrc + gpGlobals->v_right * 8;
		break;
	case 4:
		vecSrc = vecSrc + gpGlobals->v_up * -8;
		vecSrc = vecSrc + gpGlobals->v_right * 8;
		break;
	case 5:
		vecSrc = vecSrc + gpGlobals->v_up * -8;
		break;
	case 6:
		vecSrc = vecSrc + gpGlobals->v_up * -8;
		vecSrc = vecSrc + gpGlobals->v_right * -8;
		break;
	case 7:
		vecSrc = vecSrc + gpGlobals->v_right * -8;
		break;
	case 8:
		vecSrc = vecSrc + gpGlobals->v_up * 8;
		vecSrc = vecSrc + gpGlobals->v_right * -8;
		m_iFirePhase = 0;
		break;
	}

	CBaseEntity* pHornet = CBaseEntity::Create("hornet", vecSrc, m_pOperator->pev->v_angle, m_pOperator->edict());
	pHornet->pev->velocity = gpGlobals->v_forward * 1200;
	pHornet->pev->angles = UTIL_VecToAngles(pHornet->pev->velocity);

	pHornet->SetThink(&CHornet::StartDart);

	m_flRechargeTime = RANDOM_FLOAT(.1f, .9f);
#endif

	if (m_pPlayer)
	{
		int flags;
#if defined( CLIENT_WEAPONS )
		flags = FEV_NOTHOST;
#else
		flags = 0;
#endif

		PLAYBACK_EVENT_FULL(flags, m_pPlayer->edict(), m_usHornetFire, 0.0, (float*)&g_vecZero, (float*)&g_vecZero, 0.0,
		                    0.0, FIREMODE_FAST, 0, 0, 0);
		m_pPlayer->m_iWeaponVolume = NORMAL_GUN_VOLUME;
		m_pPlayer->m_iWeaponFlash = DIM_GUN_FLASH;

		// player "shoot" animation
		m_pPlayer->SetAnimation(PLAYER_ATTACK1);
	}

	GetPrimaryAmmoLeft()--;

	m_flNextPrimaryAttack = m_flNextSecondaryAttack = WeaponTimeBase() + SharedRandom(1.f / 20, 1.f / 10);
	m_flTimeWeaponIdle = WeaponTimeBase() + SharedRandom(10, 15);
}


void CHgun::Reload(void)
{
	const auto maxCarry = FindPrimaryAmmoType()->maxCarry;
	if (GetPrimaryAmmoLeft() >= maxCarry)
		return;

	while (GetPrimaryAmmoLeft() < maxCarry && m_flRechargeTime < gpGlobals->time)
	{
		GetPrimaryAmmoLeft()++;
		m_flRechargeTime += RANDOM_FLOAT(.1f, .9f);
	}
}


void CHgun::WeaponIdle(void)
{
	Reload();

	if (m_flTimeWeaponIdle > WeaponTimeBase())
		return;

	int iAnim;
	float flRand = SharedRandom(0, 1);
	if (flRand <= .75f)
	{
		iAnim = HGUN_IDLE1;
		m_flTimeWeaponIdle = WeaponTimeBase() + 30.f / 16 * (2);
	}
	else if (flRand <= .875f)
	{
		iAnim = HGUN_FIDGETSWAY;
		m_flTimeWeaponIdle = WeaponTimeBase() + 40.f / 16.f;
	}
	else
	{
		iAnim = HGUN_FIDGETSHAKE;
		m_flTimeWeaponIdle = WeaponTimeBase() + 35.f / 16.f;
	}
	SendWeaponAnim(iAnim);
}

#endif
