#include "extdll.h"
#include "util.h"
#include "cbase.h"
#include "monsters.h"
#include "weapons.h"
#include "nodes.h"
#include "player.h"
#include "soundent.h"
#include "gamerules.h"

enum m249_e
{
	M249_LONGIDLE = 0,
	M249_IDLE1,
	M249_RELOAD_1,
	M249_RELOAD_2,
	M249_HOLSTER,
	M249_DRAW,
	M249_FIRE1,
	M249_FIRE2,
	M249_FIRE3,
};



LINK_ENTITY_TO_CLASS(weapon_m249, CM249);
LINK_ENTITY_TO_CLASS(weapon_saw, CM249);

int CM249::GetRoundsBody() const
{
	if (m_iClip >= 8)
		return 0;
	if (m_iClip <= 0)
		return 8;
	return 8 - m_iClip;
}


void CM249::Spawn( )
{
	pev->classname = MAKE_STRING("weapon_m249");
	Precache( );
	SET_MODEL(ENT(pev), "models/weapons/w_m249.mdl");
	m_iId = GetWeaponId();

	m_iDefaultAmmo = GetMaxClip();

	FallInit();// get ready to fall down.
}

const char* szM249PrimaryFireSounds[] = {
	"weapons/saw_fire1.wav",
	"weapons/saw_fire2.wav",
	"weapons/saw_fire3.wav"
};

const char* CM249::GetPrimaryFireSound() const
{
	return szM249PrimaryFireSounds[RANDOM_LONG(0, ARRAYSIZE(szM249PrimaryFireSounds)-1)];
}

void CM249::Precache( void )
{
	CBasePlayerWeapon::Precache();
	
	PRECACHE_MODEL("models/weapons/v_m249.mdl");
	PRECACHE_MODEL("models/weapons/w_m249.mdl");
	PRECACHE_MODEL("models/weapons/p_m249.mdl");

	UTIL_PrecacheOther("ammo_m249_clip");
	PRECACHE_SOUND("items/9mmclip1.wav");              

	PRECACHE_SOUND("weapons/saw_reload.wav");
	PRECACHE_SOUND("weapons/saw_reload2.wav");
	
	for (auto& szPrimaryFireSound : szM249PrimaryFireSounds)
	{
		PRECACHE_SOUND(szPrimaryFireSound);
	}

	PRECACHE_SOUND ("weapons/357_cock1.wav");
	m_usPrimaryFire = PRECACHE_EVENT( 1, "events/m249-1.sc" );
}


bool CM249::AddToPlayer(CBasePlayer* pPlayer)
{
	if ( CBasePlayerWeapon::AddToPlayer( pPlayer ) )
	{
		MESSAGE_BEGIN( MSG_ONE, gmsgWeapPickup, NULL, pPlayer->pev );
			WRITE_BYTE( m_iId );
		MESSAGE_END();
		return TRUE;
	}
	return FALSE;
}

BOOL CM249::Deploy( )
{
	return DefaultDeploy( "models/weapons/v_m249.mdl", "models/weapons/p_m249.mdl", M249_DRAW, "mp5", 0, GetRoundsBody());
}

void CM249::Holster(int skiplocal)
{
	m_pOperator->m_flNextAttack = WeaponTimeBase() + 1.f;
	m_flTimeWeaponIdle = SharedRandom(10, 15);
	pev->body = GetRoundsBody();
	SendWeaponAnim(M249_HOLSTER, 1, pev->body);
	CBasePlayerWeapon::Holster(skiplocal);
}


bool CM249::PrimaryAttack()
{
	pev->body = GetRoundsBody();
	Vector vecDir;
	if (!DefaultPrimaryFire(vecDir, GetSpread(12/60.0f), 800, AUTOAIM_10DEGREES,
	                        2, -1.5f, .5f, 1.25f))
		return false;

	if (m_pPlayer)
	{
#if defined( CLIENT_WEAPONS )
		int flags = FEV_NOTHOST;
#else
		int flags = 0;
#endif

		
		PLAYBACK_EVENT_FULL(flags, m_pOperator->edict(), m_usPrimaryFire, 0.0, (float*)& g_vecZero, (float*)& g_vecZero, vecDir.x, vecDir.y, pev->body, 0, 0, 0);
	}
	return true;
}



void CM249::Reload( void )
{
	pev->body = GetRoundsBody();
	auto& ammoLeft = GetPrimaryAmmoLeft();
	if (ammoLeft <= 0 || m_iClip >= GetWeaponInfo().maxClip)
		return;

	// don't reload until recoil is done
	if (m_flNextPrimaryAttack > WeaponTimeBase())
		return;

	// check to see if we're ready to reload
	if (m_fInSpecialReload == 0)
	{
		SendWeaponAnim(M249_RELOAD_1, 1, pev->body);
		m_fInSpecialReload = 1;
		
		const auto animLength = 61 / 40.f;
		m_pOperator->m_flNextAttack = WeaponTimeBase() + animLength;
		m_flTimeWeaponIdle = WeaponTimeBase() + animLength;
		m_flNextPrimaryAttack = GetNextAttackDelay(animLength + 1);
		m_flNextSecondaryAttack = WeaponTimeBase() + animLength + 1;
		return;
	}

	if (m_fInSpecialReload == 1)
	{
		if (m_flTimeWeaponIdle > WeaponTimeBase())
			return;
		// was waiting for gun to move to side
		m_fInSpecialReload = 2;

		SendWeaponAnim(M249_RELOAD_2);
		const auto animLength = 111 / 45.f;
		m_pOperator->m_flNextAttack = WeaponTimeBase() + animLength;
		m_flTimeWeaponIdle = WeaponTimeBase() + animLength;
		m_flNextPrimaryAttack = GetNextAttackDelay(animLength);
		m_flNextSecondaryAttack = WeaponTimeBase() + animLength;
	}
	else
	{
		auto ammoToTransfer = V_min(GetMaxClip() - m_iClip, ammoLeft);
		m_iClip += ammoToTransfer;
		ammoLeft -= ammoToTransfer;
		m_fInSpecialReload = 0;
	}
}


void CM249::WeaponIdle( void )
{
	pev->body = GetRoundsBody();
	auto& ammoLeft = GetPrimaryAmmoLeft();
	ResetEmptySound();

	Aim(AUTOAIM_10DEGREES);

	if (m_flTimeWeaponIdle < WeaponTimeBase())
	{
		if (m_iClip <= 0 && m_fInSpecialReload == 0 && ammoLeft > 0)
		{
			Reload();
		}
		else if (m_fInSpecialReload != 0)
		{
			if (m_iClip < GetMaxClip() && ammoLeft > 0)
			{
				Reload();
			}
			else
			{				
				m_fInSpecialReload = 0;
				m_flTimeWeaponIdle = WeaponTimeBase() + 1.5f;
			}
		}
		else
		{
			int iAnim;
			const float flRand = SharedRandom(0, 1);
			if (flRand <= .90f)
			{
				iAnim = M249_LONGIDLE;
				m_flTimeWeaponIdle = WeaponTimeBase() + (101 / 20.f);
			}
			else
			{
				iAnim = M249_IDLE1;
				m_flTimeWeaponIdle = WeaponTimeBase() + (155 / 25.f);
			}
			SendWeaponAnim(iAnim, 1, pev->body);
		}
	}
}
