/***
*
*	Copyright (c) 1996-2001, Valve LLC. All rights reserved.
*	
*	This product contains software technology licensed from Id 
*	Software, Inc. ("Id Technology").  Id Technology (c) 1996 Id Software, Inc. 
*	All Rights Reserved.
*
*   Use, distribution, and modification of this source code and/or resulting
*   object code is restricted to non-commercial enhancements to products from
*   Valve LLC.  All other use, distribution, or modification is prohibited
*   without written permission from Valve LLC.
*
****/

#include "extdll.h"
#include "util.h"
#include "cbase.h"
#include "monsters.h"
#include "weapons.h"
#include "nodes.h"
#include "player.h"
#include "shell_types.h"

enum glock_e {
	GLOCK_LONGIDLE = 0,
	GLOCK_IDLE1,
	GLOCK_IDLE2,
	GLOCK_IDLE3,
	GLOCK_SHOOT,
	GLOCK_SHOOT_EMPTY,
	GLOCK_RELOAD,
	GLOCK_RELOAD_NOT_EMPTY,
	GLOCK_DRAW
};


const char* CGlock::szWorldModel = "models/weapons/w_glock.mdl";
const char* CGlock::szViewModel = "models/weapons/v_glock.mdl";
const char* CGlock::szPlayerModel = "models/weapons/p_glock.mdl";

LINK_ENTITY_TO_CLASS( weapon_glock, CGlock );
LINK_ENTITY_TO_CLASS( weapon_9mmhandgun, CGlock );


void CGlock::Spawn( )
{
	pev->classname = MAKE_STRING("weapon_9mmhandgun"); // hack to allow for old names
	Precache( );
	m_iId = GetWeaponInfo().id;
	SET_MODEL(ENT(pev), szWorldModel);

	m_iDefaultAmmo = GetWeaponInfo().maxClip;

	FallInit();// get ready to fall down.
}

const char* CGlock::GetPrimaryFireSound() const
{
	return "weapons/glock-fire-1.wav";
}

void CGlock::Precache( void )
{
	CBasePlayerWeapon::Precache();
	
	PRECACHE_MODEL(szViewModel);
	PRECACHE_MODEL(szWorldModel);
	PRECACHE_MODEL(szPlayerModel);
	
	PRECACHE_SOUND("items/9mmclip1.wav");
	PRECACHE_SOUND("items/9mmclip2.wav");

	PRECACHE_SOUND ("weapons/pl_gun1.wav");//silenced handgun
	PRECACHE_SOUND ("weapons/pl_gun2.wav");//silenced handgun
	PRECACHE_SOUND ("weapons/glock-fire-1.wav");//handgun

	m_usFireGlock1 = PRECACHE_EVENT( 1, "events/glock1.sc" );
	m_usFireGlock2 = PRECACHE_EVENT( 1, "events/glock2.sc" );
}

BOOL CGlock::Deploy( )
{
	// pev->body = 1;
	return DefaultDeploy( szViewModel, szPlayerModel, GLOCK_DRAW, "onehanded", /*UseDecrement() ? 1 : 0*/ 0 );
}

void CGlock::SecondaryAttack( void )
{
	GlockFire( 0.02, 60/.1f, FALSE );
}

bool CGlock::PrimaryAttack( void )
{
	return GlockFire( 0.005, 60/.3f, TRUE );
}

bool CGlock::GlockFire(float spread, float shotsPerMinute, BOOL useAutoAim)
{
	Vector vecDir;
	if (!DefaultPrimaryFire(vecDir, spread, shotsPerMinute, useAutoAim ? AUTOAIM_10DEGREES : 0))
		return false;
	if (m_pPlayer)
	{
		// silenced
		if (pev->body == 1)
		{
			m_pPlayer->m_iWeaponVolume = QUIET_GUN_VOLUME;
			m_pPlayer->m_iWeaponFlash = DIM_GUN_FLASH;
		}
#if defined( CLIENT_WEAPONS )
		int flags = FEV_NOTHOST;
#else
			int flags = 0;
#endif
		PLAYBACK_EVENT_FULL(flags, m_pOperator->edict(), 
		                    useAutoAim ? m_usFireGlock1 : m_usFireGlock2, 
		                    0.0, 
		                    (float*)& g_vecZero,
		                    (float*)& g_vecZero, 
		                    vecDir.x, vecDir.y,
		                    0, 0, (m_iClip == 0) ? 1 : 0, 0);
	}
	return true;
}


void CGlock::Reload( void )
{
	if ( GetPrimaryAmmoLeft() <= 0 )
		 return;

	int iResult;

	if (m_iClip == 0)
		iResult = DefaultReload( GLOCK_RELOAD, 1.5);
	else
		iResult = DefaultReload( GLOCK_RELOAD_NOT_EMPTY, 1.5);

	if (iResult)
	{
		m_flTimeWeaponIdle = WeaponTimeBase() + SharedRandom(10, 15);
	}
}



void CGlock::WeaponIdle( void )
{
	ResetEmptySound( );

	if (m_pPlayer)
	{
		m_pPlayer->GetAutoaimVector(AUTOAIM_10DEGREES);
	}

	if ( m_flTimeWeaponIdle > WeaponTimeBase() )
		return;

	// only idle if the slid isn't back
	if (m_iClip != 0)
	{
		int iAnim;
		float flRand = SharedRandom(0.0, 1.0 );

		if (flRand <= 0.3 + 0 * 0.75)
		{
			iAnim = GLOCK_IDLE3;
			m_flTimeWeaponIdle = WeaponTimeBase() + 49.0 / 16;
		}
		else if (flRand <= 0.6 + 0 * 0.875)
		{
			iAnim = GLOCK_IDLE1;
			m_flTimeWeaponIdle = WeaponTimeBase() + 60.0 / 16.0;
		}
		else
		{
			iAnim = GLOCK_IDLE2;
			m_flTimeWeaponIdle = WeaponTimeBase() + 40.0 / 16.0;
		}
		SendWeaponAnim( iAnim, 1 );
	}
}