/***
*
*	Copyright (c) 1996-2001, Valve LLC. All rights reserved.
*	
*	This product contains software technology licensed from Id 
*	Software, Inc. ("Id Technology").  Id Technology (c) 1996 Id Software, Inc. 
*	All Rights Reserved.
*
*   Use, distribution, and modification of this source code and/or resulting
*   object code is restricted to non-commercial enhancements to products from
*   Valve LLC.  All other use, distribution, or modification is prohibited
*   without written permission from Valve LLC.
*
****/

#include "extdll.h"
#include "util.h"
#include "cbase.h"
#include "monsters.h"
#include "weapons.h"
#include "nodes.h"
#include "player.h"
#include "gamerules.h"

// special deathmatch shotgun spreads
#define VECTOR_CONE_DM_SHOTGUN	Vector( 0.08716, 0.04362, 0.00  )// 10 degrees by 5 degrees
#define VECTOR_CONE_DM_DOUBLESHOTGUN Vector( 0.17365, 0.04362, 0.00 ) // 20 degrees by 5 degrees

enum shotgun_e {
	SHOTGUN_IDLE = 0,
	SHOTGUN_FIRE,
	SHOTGUN_FIRE2,
	SHOTGUN_RELOAD,
	SHOTGUN_PUMP,
	SHOTGUN_START_RELOAD,
	SHOTGUN_DRAW,
	SHOTGUN_HOLSTER,
	SHOTGUN_IDLE4,
	SHOTGUN_IDLE_DEEP
};

LINK_ENTITY_TO_CLASS( weapon_shotgun, CShotgun );

void CShotgun::Spawn( )
{
	Precache( );
	m_iId = WEAPON_SHOTGUN;
	SET_MODEL(ENT(pev), "models/w_shotgun.mdl");

	m_iDefaultAmmo = SHOTGUN_DEFAULT_GIVE;

	FallInit();// get ready to fall
}

const char* CShotgun::GetPrimaryFireSound() const
{
	return "weapons/sbarrel1.wav";
}

void CShotgun::Precache( void )
{
	CBasePlayerWeapon::Precache();
	
	PRECACHE_MODEL("models/v_shotgun.mdl");
	PRECACHE_MODEL("models/w_shotgun.mdl");
	PRECACHE_MODEL("models/p_shotgun.mdl");

	PRECACHE_SOUND("items/9mmclip1.wav");              

	PRECACHE_SOUND ("weapons/dbarrel1.wav");//shotgun
	PRECACHE_SOUND ("weapons/sbarrel1.wav");//shotgun

	PRECACHE_SOUND ("weapons/reload1.wav");	// shotgun reload
	PRECACHE_SOUND ("weapons/reload3.wav");	// shotgun reload
	
	PRECACHE_SOUND ("weapons/357_cock1.wav"); // gun empty sound
	PRECACHE_SOUND ("weapons/scock1.wav");	// cock gun

	m_usSingleFire = PRECACHE_EVENT( 1, "events/shotgun1.sc" );
	m_usDoubleFire = PRECACHE_EVENT( 1, "events/shotgun2.sc" );
}

bool CShotgun::AddToPlayer(CBasePlayer* pPlayer)
{
	if ( CBasePlayerWeapon::AddToPlayer( pPlayer ) )
	{
		MESSAGE_BEGIN( MSG_ONE, gmsgWeapPickup, NULL, pPlayer->pev );
			WRITE_BYTE( m_iId );
		MESSAGE_END();
		return TRUE;
	}
	return FALSE;
}



BOOL CShotgun::Deploy( )
{
	return DefaultDeploy( "models/v_shotgun.mdl", "models/p_shotgun.mdl", SHOTGUN_DRAW, "shotgun" );
}

bool CShotgun::PrimaryAttack()
{
	Vector vecDir;
	if (!DefaultPrimaryFire(vecDir, 0.050f, 60/.75f, AUTOAIM_10DEGREES, 2, -5.f, .6f, 1.f))
		return false;

	if (m_pPlayer)
	{
		m_pPlayer->m_iWeaponVolume = LOUD_GUN_VOLUME;
		
#if defined( CLIENT_WEAPONS )
		int flags = FEV_NOTHOST;
#else
		int flags = 0;
#endif

		PLAYBACK_EVENT_FULL(flags, m_pOperator->edict(), m_usSingleFire, 0.0, (float*)& g_vecZero, (float*)& g_vecZero, vecDir.x, vecDir.y, 0, 0, 0, 0);
	}
	
	if (m_iClip != 0)
		m_flPumpTime = WeaponTimeBase() + 0.5;

	if (m_iClip != 0)
		m_flTimeWeaponIdle = WeaponTimeBase() + 5.0;
	else
		m_flTimeWeaponIdle = WeaponTimeBase() + 0.75;
	m_fInSpecialReload = 0;
	return true;
}


void CShotgun::SecondaryAttack( void )
{
	PlayEmptySound();
	m_flNextPrimaryAttack = GetNextAttackDelay(0.15);
}


void CShotgun::Reload( void )
{
	auto& ammoLeft = GetPrimaryAmmoLeft();
	if (ammoLeft <= 0 || m_iClip >= GetWeaponInfo().maxClip)
		return;

	// don't reload until recoil is done
	if (m_flNextPrimaryAttack > WeaponTimeBase())
		return;

	// check to see if we're ready to reload
	if (m_fInSpecialReload == 0)
	{
		SendWeaponAnim( SHOTGUN_START_RELOAD );
		m_fInSpecialReload = 1;
		m_pOperator->m_flNextAttack = WeaponTimeBase() + .6f;
		m_flTimeWeaponIdle = WeaponTimeBase() + .6f;
		m_flNextPrimaryAttack = GetNextAttackDelay(1.f);
		m_flNextSecondaryAttack = WeaponTimeBase() + 1.f;
		return;
	}

	if (m_fInSpecialReload == 1)
	{
		if (m_flTimeWeaponIdle > WeaponTimeBase())
			return;
		// was waiting for gun to move to side
		m_fInSpecialReload = 2;

		if (RANDOM_LONG(0,1))
			EMIT_SOUND_DYN(ENT(m_pOperator->pev), CHAN_ITEM, "weapons/reload1.wav", 1, ATTN_NORM, 0, 85 + RANDOM_LONG(0,0x1f));
		else
			EMIT_SOUND_DYN(ENT(m_pOperator->pev), CHAN_ITEM, "weapons/reload3.wav", 1, ATTN_NORM, 0, 85 + RANDOM_LONG(0,0x1f));

		SendWeaponAnim( SHOTGUN_RELOAD );

		m_flNextReload = WeaponTimeBase() + .5f;
		m_flTimeWeaponIdle = WeaponTimeBase() + .5f;
	}
	else
	{
		// Add them to the clip
		m_iClip += 1;
		ammoLeft -= 1;
		m_fInSpecialReload = 1;
	}
}


void CShotgun::WeaponIdle( void )
{
	auto& ammoLeft = GetPrimaryAmmoLeft();
	ResetEmptySound( );

	Aim( AUTOAIM_10DEGREES );

	if ( m_flPumpTime && m_flPumpTime < gpGlobals->time )
	{
		// play pumping sound
		EMIT_SOUND_DYN(ENT(m_pOperator->pev), CHAN_ITEM, "weapons/scock1.wav", 1, ATTN_NORM, 0, 95 + RANDOM_LONG(0,0x1f));
		m_flPumpTime = 0;
	}

	if (m_flTimeWeaponIdle <  WeaponTimeBase() )
	{
		if (m_iClip <= 0 && m_fInSpecialReload == 0 && ammoLeft > 0)
		{
			Reload( );
		}
		else if (m_fInSpecialReload != 0)
		{
			if (m_iClip < GetMaxClip() && ammoLeft > 0)
			{
				Reload( );
			}
			else
			{
				// reload debounce has timed out
				SendWeaponAnim( SHOTGUN_PUMP );
				
				// play cocking sound
				EMIT_SOUND_DYN(ENT(m_pOperator->pev), CHAN_ITEM, "weapons/scock1.wav", 1, ATTN_NORM, 0, 95 + RANDOM_LONG(0,0x1f));
				m_fInSpecialReload = 0;
				m_flTimeWeaponIdle = WeaponTimeBase() + 1.5;
			}
		}
		else
		{
			int iAnim;
			float flRand = SharedRandom( 0, 1 );
			if (flRand <= 0.8)
			{
				iAnim = SHOTGUN_IDLE_DEEP;
				m_flTimeWeaponIdle = WeaponTimeBase() + (60.0/12.0);// * RANDOM_LONG(2, 5);
			}
			else if (flRand <= 0.95)
			{
				iAnim = SHOTGUN_IDLE;
				m_flTimeWeaponIdle = WeaponTimeBase() + (20.0/9.0);
			}
			else
			{
				iAnim = SHOTGUN_IDLE4;
				m_flTimeWeaponIdle = WeaponTimeBase() + (20.0/9.0);
			}
			SendWeaponAnim( iAnim );
		}
	}
}


