/***
*
*	Copyright (c) 1996-2001, Valve LLC. All rights reserved.
*	
*	This product contains software technology licensed from Id 
*	Software, Inc. ("Id Technology").  Id Technology (c) 1996 Id Software, Inc. 
*	All Rights Reserved.
*
*   Use, distribution, and modification of this source code and/or resulting
*   object code is restricted to non-commercial enhancements to products from
*   Valve LLC.  All other use, distribution, or modification is prohibited
*   without written permission from Valve LLC.
*
****/
#if !defined( OEM_BUILD ) && !defined( HLDEMO_BUILD )

#include "extdll.h"
#include "util.h"
#include "cbase.h"
#include "monsters.h"
#include "weapons.h"
#include "nodes.h"
#include "player.h"
#include "gamerules.h"
#include "entities/CrossbowBolt.h"


enum crossbow_e {
	CROSSBOW_IDLE1 = 0,	// full
	CROSSBOW_IDLE2,		// empty
	CROSSBOW_FIDGET1,	// full
	CROSSBOW_FIDGET2,	// empty
	CROSSBOW_FIRE1,		// full
	CROSSBOW_FIRE2,		// reload
	CROSSBOW_FIRE3,		// empty
	CROSSBOW_RELOAD,	// from empty
	CROSSBOW_DRAW1,		// full
	CROSSBOW_DRAW2,		// empty
	CROSSBOW_HOLSTER1,	// full
	CROSSBOW_HOLSTER2,	// empty
};

LINK_ENTITY_TO_CLASS( weapon_crossbow, CCrossbow );

void CCrossbow::Spawn( )
{
	Precache( );
	m_iId = WEAPON_CROSSBOW;
	SET_MODEL(ENT(pev), "models/w_crossbow.mdl");

	m_iDefaultAmmo = CROSSBOW_DEFAULT_GIVE;

	FallInit();// get ready to fall down.
}

bool CCrossbow::AddToPlayer(CBasePlayer* pPlayer)
{
	if ( CBasePlayerWeapon::AddToPlayer( pPlayer ) )
	{
		MESSAGE_BEGIN( MSG_ONE, gmsgWeapPickup, NULL, pPlayer->pev );
			WRITE_BYTE( m_iId );
		MESSAGE_END();
		return TRUE;
	}
	return FALSE;
}

void CCrossbow::Precache( void )
{
	CBasePlayerWeapon::Precache();
	
	PRECACHE_MODEL("models/w_crossbow.mdl");
	PRECACHE_MODEL("models/v_crossbow.mdl");
	PRECACHE_MODEL("models/p_crossbow.mdl");

	PRECACHE_SOUND(GetPrimaryFireSound());
	PRECACHE_SOUND("weapons/xbow_reload1.wav");

	UTIL_PrecacheOther( "crossbow_bolt" );

	m_usCrossbow = PRECACHE_EVENT( 1, "events/crossbow1.sc" );
	m_usCrossbow2 = PRECACHE_EVENT( 1, "events/crossbow2.sc" );
}

const char* CCrossbow::GetPrimaryFireSound() const
{
	return "weapons/xbow_fire1.wav";
}


BOOL CCrossbow::Deploy( )
{
	if (m_iClip)
		return DefaultDeploy( "models/v_crossbow.mdl", "models/p_crossbow.mdl", CROSSBOW_DRAW1, "bow" );
	return DefaultDeploy( "models/v_crossbow.mdl", "models/p_crossbow.mdl", CROSSBOW_DRAW2, "bow" );
}

void CCrossbow::Holster( int skiplocal /* = 0 */ )
{
	m_fInReload = FALSE;// cancel any reload in progress.

	if ( m_fInZoom )
	{
		SecondaryAttack( );
	}

	m_pOperator->m_flNextAttack = WeaponTimeBase() + 0.5;
	if (m_iClip)
		SendWeaponAnim( CROSSBOW_HOLSTER1 );
	else
		SendWeaponAnim( CROSSBOW_HOLSTER2 );
}

bool CCrossbow::PrimaryAttack( void )
{
	if (m_iClip == 0)
	{
		PlayEmptySound( );
		return false;
	}

	m_iClip--;

	if (m_pPlayer)
	{
		m_pPlayer->m_iWeaponVolume = QUIET_GUN_VOLUME;
		
#if defined( CLIENT_WEAPONS )
		const int flags = FEV_NOTHOST;
#else
		int flags = 0;
#endif
		PLAYBACK_EVENT_FULL(flags, m_pOperator->edict(), m_usCrossbow, 0.0, (float*)& g_vecZero, (float*)& g_vecZero, 0, 0, m_iClip, GetPrimaryAmmoLeft(), 0, 0);
		// player "shoot" animation
		m_pPlayer->SetAnimation(PLAYER_ATTACK1);
	}

	Vector vecDir = Aim();
	Vector anglesAim = m_pOperator->pev->v_angle + m_pOperator->pev->punchangle;
	anglesAim.x		= -anglesAim.x;
	Vector vecSrc	 = m_pOperator->GetGunPosition( ) - gpGlobals->v_up * 2;

#ifndef CLIENT_DLL
	auto pBolt = CCrossbowBolt::BoltCreate();
	pBolt->pev->origin = vecSrc;
	pBolt->pev->angles = anglesAim;
	pBolt->pev->owner = m_pOperator->edict();

	if (m_pOperator->pev->waterlevel == 3)
	{
		pBolt->pev->velocity = vecDir * BOLT_WATER_VELOCITY;
		pBolt->pev->speed = BOLT_WATER_VELOCITY;
	}
	else
	{
		pBolt->pev->velocity = vecDir * BOLT_AIR_VELOCITY;
		pBolt->pev->speed = BOLT_AIR_VELOCITY;
	}
	pBolt->pev->avelocity.z = 10;
#endif

	if (m_iClip <= 0)
	{
		if (GetPrimaryAmmoLeft() <= 0)
			m_pOperator->OnOutOfAmmo(this);
		m_pOperator->OnNeedToReload(this);
	}

	m_flNextPrimaryAttack = GetNextAttackDelay(.75f);
	m_flNextSecondaryAttack = WeaponTimeBase() + .75f;

	if (m_iClip != 0)
		m_flTimeWeaponIdle = WeaponTimeBase() + 5.f;
	else
		m_flTimeWeaponIdle = WeaponTimeBase() + .75f;
	return true;
}


void CCrossbow::SecondaryAttack()
{
	if (m_pPlayer)
	{
		if (m_pPlayer->pev->fov != 0)
		{
			m_pPlayer->pev->fov = m_pPlayer->m_iFOV = 0; // 0 means reset to default fov
			m_fInZoom = 0;
		}
		else if (m_pOperator->pev->fov != 20)
		{
			m_pPlayer->pev->fov = m_pPlayer->m_iFOV = 20;
			m_fInZoom = 1;
		}		
	}
	
	pev->nextthink = WeaponTimeBase() + 0.1f;
	m_flNextSecondaryAttack = WeaponTimeBase() + 1.f;
}


void CCrossbow::Reload( void )
{
	if (GetPrimaryAmmoLeft() <= 0 )
		return;

	if ( m_pOperator->pev->fov != 0 )
	{
		SecondaryAttack();
	}

	if ( DefaultReload( CROSSBOW_RELOAD, 4.5) )
	{
		EMIT_SOUND_DYN(ENT(m_pOperator->pev), CHAN_ITEM, "weapons/xbow_reload1.wav", RANDOM_FLOAT(0.95, 1.0), ATTN_NORM, 0, 93 + RANDOM_LONG(0,0xF));
	}
}


void CCrossbow::WeaponIdle( void )
{
	Aim(AUTOAIM_2DEGREES);  // get the autoaim vector but ignore it;  used for autoaim crosshair in DM

	ResetEmptySound( );
	
	if ( m_flTimeWeaponIdle < WeaponTimeBase() )
	{
		float flRand = SharedRandom(0, 1 );
		if (flRand <= 0.75)
		{
			if (m_iClip)
			{
				SendWeaponAnim( CROSSBOW_IDLE1 );
			}
			else
			{
				SendWeaponAnim( CROSSBOW_IDLE2 );
			}
			m_flTimeWeaponIdle = WeaponTimeBase() + SharedRandom(10, 15 );
		}
		else
		{
			if (m_iClip)
			{
				SendWeaponAnim( CROSSBOW_FIDGET1 );
				m_flTimeWeaponIdle = WeaponTimeBase() + 90.f / 30.f;
			}
			else
			{
				SendWeaponAnim( CROSSBOW_FIDGET2 );
				m_flTimeWeaponIdle = WeaponTimeBase() + 80.f / 30.f;
			}
		}
	}
}

#endif
