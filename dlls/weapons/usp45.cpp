/***
*
*	Copyright (c) 1996-2001, Valve LLC. All rights reserved.
*	
*	This product contains software technology licensed from Id 
*	Software, Inc. ("Id Technology").  Id Technology (c) 1996 Id Software, Inc. 
*	All Rights Reserved.
*
*   Use, distribution, and modification of this source code and/or resulting
*   object code is restricted to non-commercial enhancements to products from
*   Valve LLC.  All other use, distribution, or modification is prohibited
*   without written permission from Valve LLC.
*
****/

#include "extdll.h"
#include "util.h"
#include "cbase.h"
#include "monsters.h"
#include "weapons.h"
#include "nodes.h"
#include "player.h"
#include "shell_types.h"

enum usp45_e
{
	USP45_IDLE1 = 0,
	USP45_IDLE2,
	USP45_IDLE3,
	USP45_SHOOT,
	USP45_SHOOT_EMPTY,
	USP45_RELOAD,
	USP45_RELOAD_NOT_EMPTY,
	USP45_DRAW,
	USP45_HOLSTER
};

const char* CUsp45::szWorldModel = "models/weapons/w_usp45.mdl";
const char* CUsp45::szViewModel = "models/weapons/v_usp45.mdl";
const char* CUsp45::szPlayerModel = "models/weapons/p_usp45.mdl";

LINK_ENTITY_TO_CLASS(weapon_usp45, CUsp45);

void CUsp45::Spawn()
{
	pev->classname = MAKE_STRING("weapon_usp45"); // hack to allow for old names
	Precache();
	m_iId = GetWeaponId();
	SET_MODEL(ENT(pev), szWorldModel);

	m_iDefaultAmmo = GetWeaponInfo().maxClip;
	FallInit(); // get ready to fall down.
}

const char* CUsp45::GetPrimaryFireSound() const { return "weapons/usp45-fire-1.wav"; }

void CUsp45::Precache()
{
	CBasePlayerWeapon::Precache();

	PRECACHE_MODEL(szWorldModel);
	PRECACHE_MODEL(szViewModel);
	PRECACHE_MODEL(szPlayerModel);

	PRECACHE_SOUND(GetPrimaryFireSound()); 
	PRECACHE_SOUND("weapons/match_magin.wav");
	PRECACHE_SOUND("weapons/match_magout.wav");
	PRECACHE_SOUND("weapons/match _slideback.wav");
	PRECACHE_SOUND("weapons/match_slide.wav");

	m_usFire = PRECACHE_EVENT(1, "events/usp45-1.sc");
}

BOOL CUsp45::Deploy()
{
	return DefaultDeploy(szViewModel, szPlayerModel, USP45_DRAW, "onehanded", 0);
}

bool CUsp45::PrimaryAttack()
{
	Vector vecDir;
	if (!DefaultPrimaryFire(vecDir, VECTOR_CONE_1DEGREES[0], 200, AUTOAIM_10DEGREES))
		return false;

	if (m_pPlayer)
	{
#if defined( CLIENT_WEAPONS )
		int flags = FEV_NOTHOST;
#else
			int flags = 0;
#endif
		PLAYBACK_EVENT_FULL(flags, m_pOperator->edict(),
		                    m_usFire,
		                    0.0,
		                    (float*)&g_vecZero,
		                    (float*)&g_vecZero,
		                    vecDir.x, vecDir.y,
		                    0, 0, (m_iClip == 0) ? 1 : 0, 0);
	}
	return true;
}


void CUsp45::Reload()
{
	if (GetPrimaryAmmoLeft() <= 0)
		return;

	int iResult;

	if (m_iClip == 0)
		iResult = DefaultReload(USP45_RELOAD, 1.5);
	else
		iResult = DefaultReload(USP45_RELOAD_NOT_EMPTY, 1.5);

	if (iResult) { m_flTimeWeaponIdle = WeaponTimeBase() + SharedRandom(10, 15); }
}


void CUsp45::WeaponIdle()
{
	ResetEmptySound();

	if (m_pPlayer) { m_pPlayer->GetAutoaimVector(AUTOAIM_10DEGREES); }

	if (m_flTimeWeaponIdle > WeaponTimeBase())
		return;

	// only idle if the slid isn't back
	if (m_iClip != 0)
	{
		int iAnim;
		float flRand = SharedRandom(0.0, 1.0);

		if (flRand <= 0.3 + 0 * 0.75)
		{
			iAnim = USP45_IDLE3;
			m_flTimeWeaponIdle = WeaponTimeBase() + 49 / 16.f;
		}
		else if (flRand <= 0.6 + 0 * 0.875)
		{
			iAnim = USP45_IDLE1;
			m_flTimeWeaponIdle = WeaponTimeBase() + 60 / 16.f;
		}
		else
		{
			iAnim = USP45_IDLE2;
			m_flTimeWeaponIdle = WeaponTimeBase() + 40 / 16.f;
		}
		SendWeaponAnim(iAnim, 1);
	}
}
