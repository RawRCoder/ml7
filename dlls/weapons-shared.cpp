#include "extdll.h"
#include "util.h"
#include "cbase.h"
#include "monsters.h"
#include "player.h"
#include "weapons.h"
#include "shell_types.h"
#include "soundent.h"


void CBasePlayerWeapon::Precache()
{
	m_iShell = PrecacheShellModel();
	m_iShellSecondary = PrecacheSecondaryShellModel();
}

bool CBasePlayerWeapon::DefaultPrimaryFire(Vector& outDirection, float spread, float shotsPerMinute, float autoAimDelta,
                                           float barrelLengthK, float minPunchV, float maxPunchV,
                                           float maxPunchH)
{

	if (!GetWeaponInfo().canFireUnderwater && m_pOperator->pev->waterlevel == 3)
	{
		PlayEmptySound();
		m_flNextPrimaryAttack = GetNextAttackDelay(0.15);
		return false;
	}

	if (m_iClip <= 0)
	{
		if (m_fFireOnEmpty)
		{
			PlayEmptySound();
			m_flNextPrimaryAttack = GetNextAttackDelay(0.2f);
		}
		m_pOperator->OnNeedToReload(this);
		return false;
	}

	m_iClip--;
	m_pOperator->pev->effects = (int)(m_pOperator->pev->effects) | EF_MUZZLEFLASH;

	if (m_pPlayer)
	{
		// player "shoot" animation
		m_pPlayer->SetAnimation(PLAYER_ATTACK1);
		m_pPlayer->m_iWeaponVolume = NORMAL_GUN_VOLUME;
		m_pPlayer->m_iWeaponFlash = NORMAL_GUN_FLASH;
	}

	const Vector vecSrc = m_pOperator->GetGunPosition();
	const auto vecAiming = Aim(autoAimDelta);

	outDirection = m_pOperator->FireBullets(
		FindPrimaryAmmoType()->id,
		vecSrc,
		vecAiming,
		Vector(spread, spread, spread),
		barrelLengthK,
		m_pOperator->pev,
		m_pPlayer ? m_pPlayer->random_seed : 0, !!m_pPlayer);

	m_pOperator->pev->punchangle.x += RANDOM_FLOAT(minPunchV, maxPunchV);
	m_pOperator->pev->punchangle.y += RANDOM_FLOAT(-maxPunchH, maxPunchH);

	m_flNextPrimaryAttack = m_flNextSecondaryAttack = GetNextAttackDelay(60.0f / shotsPerMinute);

	if (m_iClip <= 0)
	{
		if (GetPrimaryAmmoLeft() <= 0)
			m_pOperator->OnOutOfAmmo(this);
		m_pOperator->OnNeedToReload(this);
	}

	m_flTimeWeaponIdle = WeaponTimeBase() + SharedRandom(10, 15);
	if (!m_pPlayer)
	{
#ifndef CLIENT_DLL
		auto shellTypeId = FindPrimaryAmmoType()->shellTypeId;
		if (shellTypeId != Shell_None)
		{
			auto shellType = ShellTypesRepository::Get()->GetShellTypeById(shellTypeId);
			auto shellSound = shellType.bounceSoundType;
			
			Vector vecEjector, vecEjectorAngles;
			UTIL_MakeVectors(m_pOperator->pev->angles);
			m_pOperator->GetAttachment(1, vecEjector, vecEjectorAngles);
			const auto vecShellVelocity
				= gpGlobals->v_right * RANDOM_FLOAT(40, 50)
				+ gpGlobals->v_up * RANDOM_FLOAT(75, 200)
				+ gpGlobals->v_forward * RANDOM_FLOAT(-30, 30);
			EjectBrass(vecEjector, vecShellVelocity, pev->angles.y, m_iShell, shellSound);
		}

		const auto pSample = GetPrimaryFireSound();
		if (pSample)
			EMIT_SOUND(ENT(m_pOperator->pev), CHAN_WEAPON, pSample, 1, ATTN_NORM);
		CSoundEnt::InsertSound(bits_SOUND_COMBAT, m_pOperator->pev->origin, 384, 0.3);
#endif
	}
	
	return true;
}

float CBasePlayerWeapon::SharedRandom(float min, float max) const
{
	return m_pPlayer
		? UTIL_SharedRandomFloat(m_pPlayer->random_seed, min, max)
		: RANDOM_FLOAT(min, max);
}

Vector CBasePlayerWeapon::Aim(float autoAimDelta) const
{
	if (m_pPlayer)
	{
		if (autoAimDelta > 0)
			return m_pPlayer->GetAutoaimVector(autoAimDelta);

		UTIL_MakeVectors(m_pPlayer->pev->v_angle + m_pPlayer->pev->punchangle);
	}
	else
	{
		auto direction = m_pOperator->ShootAtEnemy(m_pOperator->GetGunPosition());
		auto angles = UTIL_VecToAngles(direction);
		m_pOperator->pev->v_angle = angles;
		m_pOperator->pev->v_angle.x *= -1;
		UTIL_MakeVectors(angles + m_pOperator->pev->punchangle);
		return direction;
	}
	return gpGlobals->v_forward;
}

int CBasePlayerWeapon::PrecacheShellModel() const
{
	const auto pAmmoType = FindPrimaryAmmoType();
	if (pAmmoType)
	{
		const auto shellTypeId = pAmmoType->shellTypeId;
		if (shellTypeId > Shell_None)
		{
			const auto shellType = ShellTypesRepository::Get()->GetShellTypeById(shellTypeId);
			return shellType.Precache();
		}
	}
	return 0;
}

int CBasePlayerWeapon::PrecacheSecondaryShellModel() const
{
	const auto pAmmoType = FindSecondaryAmmoType();
	if (pAmmoType)
	{
		const auto shellTypeId = pAmmoType->shellTypeId;
		if (shellTypeId > Shell_None)
		{
			const auto shellType = ShellTypesRepository::Get()->GetShellTypeById(shellTypeId);
			return shellType.Precache();
		}
	}
	return 0;
}