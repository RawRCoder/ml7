/***
*
*	Copyright (c) 1996-2001, Valve LLC. All rights reserved.
*	
*	This product contains software technology licensed from Id 
*	Software, Inc. ("Id Technology").  Id Technology (c) 1996 Id Software, Inc. 
*	All Rights Reserved.
*
*   This source code contains proprietary and confidential information of
*   Valve LLC and its suppliers.  Access to this code is restricted to
*   persons who have executed a written SDK license with Valve.  Any access,
*   use or distribution of this code by or to any unlicensed person is illegal.
*
****/
//=========================================================
// hgrunt
//=========================================================

//=========================================================
// Hit groups!	
//=========================================================
/*

  1 - Head
  2 - Stomach
  3 - Gun

*/


#include	"extdll.h"
#include	"plane.h"
#include	"util.h"
#include	"cbase.h"
#include	"monsters.h"
#include	"schedule.h"
#include	"animation.h"
#include	"squadmonster.h"
#include	"weapons.h"
#include	"talkmonster.h"
#include	"soundent.h"
#include	"effects.h"
#include	"customentity.h"
#include "game_monster_spawning.h"

int g_fGruntQuestion; // true if an idle grunt asked a question. Cleared when someone answers.

extern DLL_GLOBAL int g_iSkillLevel;

//=========================================================
// monster-specific DEFINE's
//=========================================================
#define GRUNT_VOL						0.35		// volume of grunt sounds
#define GRUNT_ATTN						ATTN_NORM	// attenutation of grunt sentences
#define HGRUNT_LIMP_HEALTH				20
#define HGRUNT_DMG_HEADSHOT				( DMG_BULLET | DMG_CLUB )	// damage types that can kill a grunt with a single headshot.
#define HGRUNT_NUM_HEADS				2 // how many grunt heads are there? 
#define HGRUNT_MINIMUM_HEADSHOT_DAMAGE	15 // must do at least this much damage in one shot to head to score a headshot kill
#define	HGRUNT_SENTENCE_VOLUME			(float)0.35 // volume of grunt sentences

enum class BodyGroup
{
	Heads = 1,
	Guns = 2,
};

enum class Equipment
{
	HandGrenade = 1 << 0,
	M203Grenades = 1 << 1,
	AmmoM4Clips = 1 << 2,
	AmmoMp5Clips = 1 << 3,
	AmmoBuckshot = 1 << 4,
};

enum class HeadBodyPart
{
	GasMask = 0,
	CommanderWhite,
	Balaclava,
	CommanderBlack
};

//=========================================================
// Monster's Anim Events Go Here
//=========================================================
#define		HGRUNT_AE_RELOAD		( 2 )
#define		HGRUNT_AE_KICK			( 3 )
#define		HGRUNT_AE_BURST1		( 4 )
#define		HGRUNT_AE_BURST2		( 5 )
#define		HGRUNT_AE_BURST3		( 6 )
#define		HGRUNT_AE_GREN_TOSS		( 7 )
#define		HGRUNT_AE_GREN_LAUNCH	( 8 )
#define		HGRUNT_AE_GREN_DROP		( 9 )
#define		HGRUNT_AE_CAUGHT_ENEMY	( 10 ) // grunt established sight with an enemy (player only) that had previously eluded the squad.
#define		HGRUNT_AE_DROP_GUN		( 11 ) // grunt (probably dead) is dropping his mp5.

//=========================================================
// monster-specific schedule types
//=========================================================
enum
{
	SCHED_GRUNT_SUPPRESS = LAST_COMMON_SCHEDULE + 1,
	SCHED_GRUNT_ESTABLISH_LINE_OF_FIRE,
	// move to a location to set up an attack against the enemy. (usually when a friendly is in the way).
	SCHED_GRUNT_COVER_AND_RELOAD,
	SCHED_GRUNT_SWEEP,
	SCHED_GRUNT_FOUND_ENEMY,
	SCHED_GRUNT_REPEL,
	SCHED_GRUNT_REPEL_ATTACK,
	SCHED_GRUNT_REPEL_LAND,
	SCHED_GRUNT_WAIT_FACE_ENEMY,
	SCHED_GRUNT_TAKECOVER_FAILED,
	// special schedule type that forces analysis of conditions and picks the best possible schedule to recover from this type of failure.
	SCHED_GRUNT_ELOF_FAIL,
};

//=========================================================
// monster-specific tasks
//=========================================================
enum
{
	TASK_GRUNT_FACE_TOSS_DIR = LAST_COMMON_TASK + 1,
	TASK_GRUNT_SPEAK_SENTENCE,
	TASK_GRUNT_CHECK_FIRE,
};

//=========================================================
// monster-specific conditions
//=========================================================
#define bits_COND_GRUNT_NOFIRE	( bits_COND_SPECIAL1 )

class CHGrunt : public CSquadMonster
{
public:
	void Spawn() override;
	void Precache() override;
	int Classify() override;

	bool HasEquipment(Equipment equipment) const;
	void AddEquipment(Equipment equipment);
	void RemoveEquipment(Equipment equipment);

	HeadBodyPart GetCurrentHead();
	void SetCurrentHead(HeadBodyPart head);

	void SetYawSpeed() override;
	int ISoundMask() override;
	void HandleAnimEvent(MonsterEvent_t* pEvent) override;
	BOOL FCanCheckAttacks() override;
	BOOL CheckMeleeAttack1(float flDot, float flDist) override;
	BOOL CheckRangeAttack1(float flDot, float flDist) override;
	BOOL CheckRangeAttack2(float flDot, float flDist) override;
	void SetActivity(Activity NewActivity) override;
	void StartTask(Task_t* pTask) override;
	void RunTask(Task_t* pTask) override;
	void DeathSound() override;
	void PainSound() override;
	void IdleSound() override;
	Vector GetGunPosition() override;
	void PrescheduleThink() override;
	void GibMonster() override;
	void SpeakSentence();

	int m_iCurrentWeaponAmmo;
	int& GetAmmoLeft(AmmoTypeId typeId) override
	{
		// TODO: temporary cheats
		m_iCurrentWeaponAmmo = 250;
		return m_iCurrentWeaponAmmo;
	}

	int Save(CSave& save) override;
	int Restore(CRestore& restore) override;
	static TYPEDESCRIPTION m_SaveData[];

	CBaseEntity* Kick();
	Schedule_t* GetSchedule() override;
	Schedule_t* GetScheduleOfType(int Type) override;
	void TraceAttack(entvars_t* pevAttacker, float flDamage, Vector vecDir, TraceResult* ptr, int bitsDamageType,
	                 float flArmorPiercingK) override;
	int TakeDamage(entvars_t* pevInflictor, entvars_t* pevAttacker, float flDamage, int bitsDamageType) override;

	int IRelationship(CBaseEntity* pTarget) override;

	BOOL FOkToSpeak();
	void JustSpoke();

	CUSTOM_SCHEDULES;

	// checking the feasibility of a grenade toss is kind of costly, so we do it every couple of seconds,
	// not every server frame.
	float m_flNextGrenadeCheck;
	float m_flNextPainTime;
	float m_flLastEnemySightTime;

	float m_flLastShotTime;
	int m_iBurstCounter;

	int GetActualBurstCounter(float minFullRestTime = 3.0f) const
	{
		return (gpGlobals->time - m_flLastShotTime) >= minFullRestTime
			       ? 0
			       : m_iBurstCounter;
	}

	Vector m_vecTossVelocity;

	BOOL m_fThrowGrenade;
	BOOL m_fStanding;
	BOOL m_fFirstEncounter; // only put on the handsign show in the squad's first encounter.
	int m_cClipSize;

	int m_voicePitch;
	int m_iSentence;

	Vector m_vecLastEnemyBodyTargetOffset;
	bool m_hasLastEnemyBodyTargetOffset = false;

	static const char* pGruntSentences[];
	bool EquipWeapon(CBasePlayerWeapon* pWeapon, bool autoDeploy);
private:
	CBaseEntity* DropAndSpinItem(char* itemName, const Vector& origin, const Vector& angles);
	void DropAndSpinItems(char* itemName, const Vector& origin, int count);
};

LINK_ENTITY_TO_CLASS(monster_human_grunt, CHGrunt);

TYPEDESCRIPTION CHGrunt::m_SaveData[] =
{
	DEFINE_FIELD(CHGrunt, m_flNextGrenadeCheck, FIELD_TIME),
	DEFINE_FIELD(CHGrunt, m_flNextPainTime, FIELD_TIME),
	DEFINE_FIELD(CHGrunt, m_vecTossVelocity, FIELD_VECTOR),
	DEFINE_FIELD(CHGrunt, m_fThrowGrenade, FIELD_BOOLEAN),
	DEFINE_FIELD(CHGrunt, m_fStanding, FIELD_BOOLEAN),
	DEFINE_FIELD(CHGrunt, m_fFirstEncounter, FIELD_BOOLEAN),
	DEFINE_FIELD(CHGrunt, m_cClipSize, FIELD_INTEGER),
	DEFINE_FIELD(CHGrunt, m_voicePitch, FIELD_INTEGER),
	DEFINE_FIELD(CHGrunt, m_iBurstCounter, FIELD_INTEGER),
	DEFINE_FIELD(CHGrunt, m_flLastShotTime, FIELD_TIME),
	DEFINE_FIELD(CHGrunt, m_iSentence, FIELD_INTEGER),
	DEFINE_FIELD(CHGrunt, m_iCurrentWeaponAmmo, FIELD_INTEGER),
};

IMPLEMENT_SAVERESTORE(CHGrunt, CSquadMonster);

const char* CHGrunt::pGruntSentences[] =
{
	"HG_GREN", // grenade scared grunt
	"HG_ALERT", // sees player
	"HG_MONSTER", // sees monster
	"HG_COVER", // running to cover
	"HG_THROW", // about to throw grenade
	"HG_CHARGE", // running out to get the enemy
	"HG_TAUNT", // say rude things
};


enum HGRUNT_SENTENCE_TYPES
{
	HGRUNT_SENT_NONE = -1,
	HGRUNT_SENT_GREN = 0,
	HGRUNT_SENT_ALERT,
	HGRUNT_SENT_MONSTER,
	HGRUNT_SENT_COVER,
	HGRUNT_SENT_THROW,
	HGRUNT_SENT_CHARGE,
	HGRUNT_SENT_TAUNT,
};

bool CHGrunt::EquipWeapon(CBasePlayerWeapon* pWeapon, bool autoDeploy)
{
	if (!CSquadMonster::EquipWeapon(pWeapon, autoDeploy))
		return false;

	// deploy it anyways, since we can not holster weapons :(
	if (!autoDeploy)
		pWeapon->Deploy();
	
	return true;
}

//=========================================================
// Speak Sentence - say your cued up sentence.
//
// Some grunt sentences (take cover and charge) rely on actually
// being able to execute the intended action. It's really lame
// when a grunt says 'COVER ME' and then doesn't move. The problem
// is that the sentences were played when the decision to TRY
// to move to cover was made. Now the sentence is played after 
// we know for sure that there is a valid path. The schedule
// may still fail but in most cases, well after the grunt has 
// started moving.
//=========================================================
void CHGrunt::SpeakSentence()
{
	if (m_iSentence == HGRUNT_SENT_NONE)
	{
		// no sentence cued up.
		return;
	}

	if (FOkToSpeak())
	{
		SENTENCEG_PlayRndSz(ENT(pev), pGruntSentences[m_iSentence], HGRUNT_SENTENCE_VOLUME, GRUNT_ATTN, 0,
		                    m_voicePitch);
		JustSpoke();
	}
}

//=========================================================
// IRelationship - overridden because Alien Grunts are 
// Human Grunt's nemesis.
//=========================================================
int CHGrunt::IRelationship(CBaseEntity* pTarget)
{
	if (FClassnameIs(pTarget->pev, "monster_alien_grunt") || (FClassnameIs(pTarget->pev, "monster_gargantua")))
	{
		return R_NM;
	}

	return CSquadMonster::IRelationship(pTarget);
}

//=========================================================
// GibMonster - make gun fly through the air.
//=========================================================
void CHGrunt::GibMonster()
{
	Vector vecGunPos;
	Vector vecGunAngles;
	GetAttachment(0, vecGunPos, vecGunAngles);
	auto vecCenter = Center();
	CBaseMonster::GibMonster();

	if (HasEquipment(Equipment::AmmoBuckshot))
	{
		DropAndSpinItems("ammo_buckshot", GetRandomVector(pev->absmin, pev->absmax), 2);		
	}
	
	if (HasEquipment(Equipment::AmmoMp5Clips))
	{
		DropAndSpinItems("ammo_9mmAR", GetRandomVector(pev->absmin, pev->absmax), 4);
	}
	
	if (HasEquipment(Equipment::AmmoM4Clips))
	{
		DropAndSpinItems("ammo_m4a1_clip", GetRandomVector(pev->absmin, pev->absmax), 4);
	}	

	if (HasEquipment(Equipment::HandGrenade))
	{
		DropAndSpinItems("weapon_handgrenade", GetRandomVector(pev->absmin, pev->absmax), 3);
	}

	if (HasEquipment(Equipment::M203Grenades))
	{
		DropAndSpinItems("ammo_m203", GetRandomVector(pev->absmin, pev->absmax), 2);
	}

}

CBaseEntity* CHGrunt::DropAndSpinItem(char* itemName, const Vector& origin, const Vector& angles)
{
	auto pGun = DropItem(itemName, origin, angles);
	if (pGun)
	{
		pGun->pev->velocity = Vector(RANDOM_FLOAT(-100, 100), RANDOM_FLOAT(-100, 100), RANDOM_FLOAT(200, 300));
		pGun->pev->avelocity = Vector(0, RANDOM_FLOAT(200, 400), 0);
	}
	return pGun;
}

void CHGrunt::DropAndSpinItems(char* itemName, const Vector& origin, const int count)
{
	for (auto i = 0; i < RANDOM_LONG(0, count); ++i)
	{
		auto vecRandom = Vector(RANDOM_FLOAT(-1, +1), RANDOM_FLOAT(-1, +1), RANDOM_FLOAT(-1, +1)) * 8;
		DropAndSpinItem(itemName, origin + vecRandom, {0, 0, 0});
	}
}


//=========================================================
// ISoundMask - Overidden for human grunts because they 
// hear the DANGER sound that is made by hand grenades and
// other dangerous items.
//=========================================================
int CHGrunt::ISoundMask()
{
	return bits_SOUND_WORLD |
		bits_SOUND_COMBAT |
		bits_SOUND_PLAYER |
		bits_SOUND_DANGER;
}

//=========================================================
// someone else is talking - don't speak
//=========================================================
BOOL CHGrunt::FOkToSpeak()
{
	// if someone else is talking, don't speak
	if (gpGlobals->time <= CTalkMonster::g_talkWaitTime)
		return false;

	if (pev->spawnflags & SF_MONSTER_GAG)
	{
		if (m_MonsterState != MONSTERSTATE_COMBAT)
		{
			// no talking outside of combat if gagged.
			return false;
		}
	}

	// if player is not in pvs, don't speak
	//	if (FNullEnt(FIND_CLIENT_IN_PVS(edict())))
	//		return false;

	return true;
}

//=========================================================
//=========================================================
void CHGrunt::JustSpoke()
{
	CTalkMonster::g_talkWaitTime = gpGlobals->time + RANDOM_FLOAT(1.5, 2.0);
	m_iSentence = HGRUNT_SENT_NONE;
}

//=========================================================
// PrescheduleThink - this function runs after conditions
// are collected and before scheduling code is run.
//=========================================================
void CHGrunt::PrescheduleThink()
{
	if (InSquad() && m_hEnemy != nullptr)
	{
		if (HasConditions(bits_COND_SEE_ENEMY))
		{
			// update the squad's last enemy sighting time.
			MySquadLeader()->m_flLastEnemySightTime = gpGlobals->time;
		}
		else
		{
			if (gpGlobals->time - MySquadLeader()->m_flLastEnemySightTime > 5)
			{
				// been a while since we've seen the enemy
				MySquadLeader()->m_fEnemyEluded = true;
			}
		}
	}
}

//=========================================================
// FCanCheckAttacks - this is overridden for human grunts
// because they can throw/shoot grenades when they can't see their
// target and the base class doesn't check attacks if the monster
// cannot see its enemy.
//
// !!!BUGBUG - this gets called before a 3-round burst is fired
// which means that a friendly can still be hit with up to 2 rounds. 
// ALSO, grenades will not be tossed if there is a friendly in front,
// this is a bad bug. Friendly machine gun fire avoidance
// will unecessarily prevent the throwing of a grenade as well.
//=========================================================
BOOL CHGrunt::FCanCheckAttacks()
{
	if (!HasConditions(bits_COND_ENEMY_TOOFAR))
	{
		return true;
	}
	return false;
}


//=========================================================
// CheckMeleeAttack1
//=========================================================
BOOL CHGrunt::CheckMeleeAttack1(float flDot, float flDist)
{
	if (m_hEnemy == nullptr)
		return false;

	CBaseMonster* pEnemy = m_hEnemy->MyMonsterPointer();
	if (!pEnemy) { return false; }

	if (flDist <= 64 && flDot >= .7f &&
		pEnemy->Classify() != CLASS_ALIEN_BIOWEAPON &&
		pEnemy->Classify() != CLASS_PLAYER_BIOWEAPON)
	{
		return true;
	}
	return false;
}

//=========================================================
// CheckRangeAttack1 - overridden for HGrunt, cause 
// FCanCheckAttacks() doesn't disqualify all attacks based
// on whether or not the enemy is occluded because unlike
// the base class, the HGrunt can attack when the enemy is
// occluded (throw grenade over wall, etc). We must 
// disqualify the machine gun attack if the enemy is occluded.
//=========================================================
BOOL CHGrunt::CheckRangeAttack1(float flDot, float flDist)
{
	auto pWeapon = GetCurrentWeapon();
	if (!pWeapon) 
		return false;

	auto weaponInfo = pWeapon->GetWeaponInfo();
	const float maxDistance = weaponInfo.GetMaxEffectiveRange();	
	if (HasConditions(bits_COND_ENEMY_OCCLUDED) || flDist > maxDistance || flDot < 0.5 || !NoFriendlyFire())
		return false;

	if (flDist < weaponInfo.aiMinSafeRange)
		return  false;

	if (flDist <= 64 && RANDOM_FLOAT(0, 9) < 3)
		return false;

	Vector targetToShootAt;
	return TryFindBestEnemyBodyTarget(GetGunPosition(), targetToShootAt);
}

//=========================================================
// CheckRangeAttack2 - this checks the Grunt's grenade
// attack. 
//=========================================================
BOOL CHGrunt::CheckRangeAttack2(float flDot, float flDist)
{
	if (!HasEquipment(Equipment::HandGrenade))
	{
		if (!HasEquipment(Equipment::M203Grenades))
			return false;

		auto pWeapon = GetCurrentWeapon();
		if (!pWeapon || pWeapon->GetWeaponId() != Weapon_M4A1)
			return false;
	}

	// if the grunt isn't moving, it's ok to check.
	if (m_flGroundSpeed != 0)
	{
		m_fThrowGrenade = false;
		return m_fThrowGrenade;
	}

	// assume things haven't changed too much since last time
	if (gpGlobals->time < m_flNextGrenadeCheck)
	{
		return m_fThrowGrenade;
	}

	if (!FBitSet(m_hEnemy->pev->flags, FL_ONGROUND) && m_hEnemy->pev->waterlevel == 0 && m_vecEnemyLKP.z > pev->absmax.z
	)
	{
		//!!!BUGBUG - we should make this check movetype and make sure it isn't FLY? Players who jump a lot are unlikely to 
		// be grenaded.
		// don't throw grenades at anything that isn't on the ground!
		m_fThrowGrenade = false;
		return m_fThrowGrenade;
	}

	Vector vecTarget;

	if (HasEquipment(Equipment::HandGrenade))
	{
		// find feet
		if (!RANDOM_LONG(0, 3))
		{
			// magically know where they are
			vecTarget = Vector(m_hEnemy->pev->origin.x, m_hEnemy->pev->origin.y, m_hEnemy->pev->absmin.z);
		}
		else
		{
			// toss it to where you last saw them
			vecTarget = m_vecEnemyLKP;
		}
		// vecTarget = m_vecEnemyLKP + (m_hEnemy->BodyTarget( pev->origin ) - m_hEnemy->pev->origin);
		// estimate position
		// vecTarget = vecTarget + m_hEnemy->pev->velocity * 2;
	}
	else
	{
		// find target
		// vecTarget = m_hEnemy->BodyTarget( pev->origin );
		vecTarget = m_vecEnemyLKP + (m_hEnemy->BodyTarget(pev->origin) - m_hEnemy->pev->origin);
		// estimate position
		if (HasConditions(bits_COND_SEE_ENEMY))
			vecTarget = vecTarget + ((vecTarget - pev->origin).Length()
				/ gSkillData.hgruntGrenadeSpeed) * m_hEnemy->pev->velocity;
	}

	// are any of my squad members near the intended grenade impact area?
	if (InSquad())
	{
		if (SquadMemberInRange(vecTarget, 256))
		{
			// crap, I might blow my own guy up. Don't throw a grenade and don't check again for a while.
			m_flNextGrenadeCheck = gpGlobals->time + 1; // one full second.
			m_fThrowGrenade = false;
		}
	}

	if ((vecTarget - pev->origin).Length2D() <= 256)
	{
		// crap, I don't want to blow myself up
		m_flNextGrenadeCheck = gpGlobals->time + 1; // one full second.
		m_fThrowGrenade = false;
		return m_fThrowGrenade;
	}


	if (HasEquipment(Equipment::HandGrenade))
	{
		const auto vecToss = VecCheckToss(pev, GetGunPosition(), vecTarget, 0.5);

		if (vecToss != g_vecZero)
		{
			m_vecTossVelocity = vecToss;

			// throw a hand grenade
			m_fThrowGrenade = true;
			// don't check again for a while.
			m_flNextGrenadeCheck = gpGlobals->time; // 1/3 second.
		}
		else
		{
			// don't throw
			m_fThrowGrenade = false;
			// don't check again for a while.
			m_flNextGrenadeCheck = gpGlobals->time + 1; // one full second.
		}
	}
	else
	{
		const auto vecToss = VecCheckThrow(pev, GetGunPosition(), vecTarget, gSkillData.hgruntGrenadeSpeed, 0.5);

		if (vecToss != g_vecZero)
		{
			m_vecTossVelocity = vecToss;

			// throw a hand grenade
			m_fThrowGrenade = true;
			// don't check again for a while.
			m_flNextGrenadeCheck = gpGlobals->time + .3f; // 1/3 second.
		}
		else
		{
			// don't throw
			m_fThrowGrenade = false;
			// don't check again for a while.
			m_flNextGrenadeCheck = gpGlobals->time + 1; // one full second.
		}
	}


	return m_fThrowGrenade;
}


//=========================================================
// TraceAttack - make sure we're not taking it in the helmet
//=========================================================
void CHGrunt::TraceAttack(entvars_t* pevAttacker, float flDamage, Vector vecDir, TraceResult* ptr, int bitsDamageType,
                          float flArmorPiercingK)
{
	// check for helmet shot
	if (ptr->iHitgroup == 11)
	{
		// make sure we're wearing one
		if (GetCurrentHead() == HeadBodyPart::GasMask && (bitsDamageType & (DMG_BULLET | DMG_SLASH | DMG_BLAST |
			DMG_CLUB)))
		{
			// absorb damage
			flDamage -= 20;
			if (flDamage <= 0)
			{
				UTIL_Ricochet(ptr->vecEndPos, 1.0);
				flDamage = 0.01;
			}
		}
		// it's head shot anyways
		ptr->iHitgroup = HITGROUP_HEAD;
	}
	CSquadMonster::TraceAttack(pevAttacker, flDamage, vecDir, ptr, bitsDamageType, flArmorPiercingK);
}


//=========================================================
// TakeDamage - overridden for the grunt because the grunt
// needs to forget that he is in cover if he's hurt. (Obviously
// not in a safe place anymore).
//=========================================================
int CHGrunt::TakeDamage(entvars_t* pevInflictor, entvars_t* pevAttacker, float flDamage, int bitsDamageType)
{
	Forget(bits_MEMORY_INCOVER);

	return CSquadMonster::TakeDamage(pevInflictor, pevAttacker, flDamage, bitsDamageType);
}

//=========================================================
// SetYawSpeed - allows each sequence to have a different
// turn rate associated with it.
//=========================================================
void CHGrunt::SetYawSpeed()
{
	int ys;

	switch (m_Activity)
	{
	case ACT_IDLE:
		ys = 180;
		break;
	case ACT_RUN:
		ys = 150;
		break;
	case ACT_WALK:
		ys = 180;
		break;
	case ACT_RANGE_ATTACK1:
		ys = 240;
		break;
	case ACT_RANGE_ATTACK2:
		ys = 240;
		break;
	case ACT_MELEE_ATTACK1:
		ys = 120;
		break;
	case ACT_MELEE_ATTACK2:
		ys = 120;
		break;
	case ACT_TURN_LEFT:
	case ACT_TURN_RIGHT:
		ys = 240;
		break;
	case ACT_GLIDE:
	case ACT_FLY:
		ys = 30;
		break;
	default:
		ys = 90;
		break;
	}

	pev->yaw_speed = ys;
}

void CHGrunt::IdleSound()
{
	if (FOkToSpeak() && (g_fGruntQuestion || RANDOM_LONG(0, 1)))
	{
		if (!g_fGruntQuestion)
		{
			// ask question or make statement
			switch (RANDOM_LONG(0, 2))
			{
			case 0: // check in
				SENTENCEG_PlayRndSz(ENT(pev), "HG_CHECK", HGRUNT_SENTENCE_VOLUME, ATTN_NORM, 0, m_voicePitch);
				g_fGruntQuestion = 1;
				break;
			case 1: // question
				SENTENCEG_PlayRndSz(ENT(pev), "HG_QUEST", HGRUNT_SENTENCE_VOLUME, ATTN_NORM, 0, m_voicePitch);
				g_fGruntQuestion = 2;
				break;
			case 2: // statement
				SENTENCEG_PlayRndSz(ENT(pev), "HG_IDLE", HGRUNT_SENTENCE_VOLUME, ATTN_NORM, 0, m_voicePitch);
				break;
			}
		}
		else
		{
			switch (g_fGruntQuestion)
			{
			case 1: // check in
				SENTENCEG_PlayRndSz(ENT(pev), "HG_CLEAR", HGRUNT_SENTENCE_VOLUME, ATTN_NORM, 0, m_voicePitch);
				break;
			case 2: // question 
				SENTENCEG_PlayRndSz(ENT(pev), "HG_ANSWER", HGRUNT_SENTENCE_VOLUME, ATTN_NORM, 0, m_voicePitch);
				break;
			}
			g_fGruntQuestion = 0;
		}
		JustSpoke();
	}
}

//=========================================================
// Classify - indicates this monster's place in the 
// relationship table.
//=========================================================
int CHGrunt::Classify()
{
	return CLASS_HUMAN_MILITARY;
}

bool CHGrunt::HasEquipment(const Equipment equipment) const
{
	return FBitSet(pev->weapons, (int)equipment);
}

void CHGrunt::AddEquipment(const Equipment equipment)
{
	pev->weapons |= static_cast<int>(equipment);
}

void CHGrunt::RemoveEquipment(const Equipment equipment)
{
	pev->weapons &= ~static_cast<int>(equipment);
}

HeadBodyPart CHGrunt::GetCurrentHead()
{
	return static_cast<HeadBodyPart>(GetBodygroup(static_cast<int>(BodyGroup::Heads)));
}

void CHGrunt::SetCurrentHead(const HeadBodyPart head)
{
	SetBodygroup(static_cast<int>(BodyGroup::Heads), static_cast<int>(head));
}

//=========================================================
//=========================================================
CBaseEntity* CHGrunt::Kick()
{
	TraceResult tr;

	UTIL_MakeVectors(pev->angles);
	Vector vecStart = pev->origin;
	vecStart.z += pev->size.z * 0.5;
	Vector vecEnd = vecStart + (gpGlobals->v_forward * 70);

	UTIL_TraceHull(vecStart, vecEnd, dont_ignore_monsters, head_hull, ENT(pev), &tr);

	if (tr.pHit)
	{
		CBaseEntity* pEntity = CBaseEntity::Instance(tr.pHit);
		return pEntity;
	}

	return nullptr;
}

//=========================================================
// GetGunPosition	return the end of the barrel
//=========================================================

Vector CHGrunt::GetGunPosition()
{
	if (m_fStanding)
	{
		return pev->origin + Vector(0, 0, 60);
	}
	return pev->origin + Vector(0, 0, 48);
}

//=========================================================
// HandleAnimEvent - catches the monster-specific messages
// that occur when tagged animation frames are played.
//=========================================================
void CHGrunt::HandleAnimEvent(MonsterEvent_t* pEvent)
{
	auto pWeapon = GetCurrentWeapon();
	switch (pEvent->event)
	{
	case HGRUNT_AE_DROP_GUN:
		DropWeapon();
		break;

	case HGRUNT_AE_RELOAD:
		if (!pWeapon)
		{
			EMIT_SOUND(ENT(pev), CHAN_WEAPON, "hgrunt/gr_reload1.wav", 1, ATTN_NORM);
			m_cAmmoLoaded = m_cClipSize;
			ClearConditions(bits_COND_NO_AMMO_LOADED);
		}
		else
		{
			pWeapon->Reload();
		}
		break;

	case HGRUNT_AE_GREN_TOSS:
		{
			UTIL_MakeVectors(pev->angles);
			// CGrenade::ShootTimed( pev, pev->origin + gpGlobals->v_forward * 34 + Vector (0, 0, 32), m_vecTossVelocity, 3.5 );
			CGrenade::ShootTimed(pev, GetGunPosition(), m_vecTossVelocity, 3.5);

			m_fThrowGrenade = false;

			if (RANDOM_FLOAT(0, 1) < 0.33f)
				RemoveEquipment(Equipment::HandGrenade);

			m_flNextGrenadeCheck = gpGlobals->time + 6;
			// wait six seconds before even looking again to see if a grenade can be thrown.
			// !!!LATER - when in a group, only try to throw grenade if ordered.
		}
		break;

	case HGRUNT_AE_GREN_LAUNCH:
		{
			EMIT_SOUND(ENT(pev), CHAN_WEAPON, "weapons/glauncher.wav", 0.8, ATTN_NORM);
			CGrenade::ShootContact(pev, GetGunPosition(), m_vecTossVelocity);
			m_fThrowGrenade = false;
			if (RANDOM_FLOAT(0, 1) < .33f)
				RemoveEquipment(Equipment::M203Grenades);

			if (g_iSkillLevel == SKILL_HARD)
				m_flNextGrenadeCheck = gpGlobals->time + RANDOM_FLOAT(2, 5);
				// wait a random amount of time before shooting again
			else
				m_flNextGrenadeCheck = gpGlobals->time + 6;
			// wait six seconds before even looking again to see if a grenade can be thrown.
		}
		break;

	case HGRUNT_AE_GREN_DROP:
		{
			if (RANDOM_FLOAT(0, 1) < .33f)
				RemoveEquipment(Equipment::HandGrenade);

			m_flNextGrenadeCheck = gpGlobals->time + 6;
			UTIL_MakeVectors(pev->angles);
			CGrenade::ShootTimed(
				pev, pev->origin + gpGlobals->v_forward * 17 - gpGlobals->v_right * 27 + gpGlobals->v_up * 6, g_vecZero,
				3);
		}
		break;

	case HGRUNT_AE_BURST1:
	case HGRUNT_AE_BURST2:
	case HGRUNT_AE_BURST3:
		{
			const auto vecShootDir = ShootAtEnemy(GetGunPosition());
			const auto angDir = UTIL_VecToAngles(vecShootDir);
			SetBlending(0, angDir.x);
			if (pWeapon)
			{
				pWeapon->PrimaryAttack();
			}
		}
		break;

	case HGRUNT_AE_KICK:
		{
			CBaseEntity* pHurt = Kick();
			if (pHurt)
			{
				// SOUND HERE!
				UTIL_MakeVectors(pev->angles);
				pHurt->pev->punchangle.x = 15;
				pHurt->pev->velocity = pHurt->pev->velocity + gpGlobals->v_forward * 100 + gpGlobals->v_up * 50;
				pHurt->TakeDamage(pev, pev, gSkillData.hgruntDmgKick, DMG_CLUB);
			}
		}
		break;

	case HGRUNT_AE_CAUGHT_ENEMY:
		{
			if (FOkToSpeak())
			{
				SENTENCEG_PlayRndSz(ENT(pev), "HG_ALERT", HGRUNT_SENTENCE_VOLUME, GRUNT_ATTN, 0, m_voicePitch);
				JustSpoke();
			}
		}

	default:
		CSquadMonster::HandleAnimEvent(pEvent);
		break;
	}
}

const char* GetRandomWeapon()
{
	if (RANDOM_FLOAT(0, 1) < .50f)
		return "weapon_m4a1";

	if (RANDOM_FLOAT(0, 1) < .50f)
		return "weapon_mp5";

	if (RANDOM_FLOAT(0, 1) < .50f)
		return "weapon_shotgun";

	if (RANDOM_FLOAT(0, 1) < .75f)
		return "weapon_m249";

	return "weapon_rpg";
}

//=========================================================
// Spawn
//=========================================================
void CHGrunt::Spawn()
{
	Precache();

	SET_MODEL(ENT(pev), "models/hgrunt.mdl");
	UTIL_SetSize(pev, VEC_HUMAN_HULL_MIN, VEC_HUMAN_HULL_MAX);

	pev->solid = SOLID_SLIDEBOX;
	pev->movetype = MOVETYPE_STEP;
	m_bloodColor = BLOOD_COLOR_RED;
	pev->effects = 0;
	pev->health = gSkillData.hgruntHealth;
	m_flFieldOfView = .25f; // indicates the width of this monster's forward view cone ( as a dotproduct result )
	m_MonsterState = MONSTERSTATE_NONE;
	m_flNextGrenadeCheck = gpGlobals->time + 1;
	m_flNextPainTime = gpGlobals->time;
	m_flLastShotTime = gpGlobals->time;
	m_iBurstCounter = 0;
	m_iSentence = HGRUNT_SENT_NONE;

	m_afCapability = bits_CAP_SQUAD | bits_CAP_TURN_HEAD | bits_CAP_DOORS_GROUP;

	m_fEnemyEluded = false;
	m_fFirstEncounter = true; // this is true when the grunt spawns, because he hasn't encountered an enemy yet.

	m_HackedGunPos = Vector(0, 0, 55);


	if (!RANDOM_LONG(0, 2))
	{
		AddEquipment(Equipment::HandGrenade);
	}

	//if (pev->weapons == 0)
	{
		auto szWeaponName = GetRandomWeapon();
		auto pWeapon = (CBasePlayerWeapon*)Create(szWeaponName, pev->origin, pev->angles);
		if (pWeapon)
		{
			EquipWeapon(pWeapon, true);
			if (pWeapon->GetWeaponId() == Weapon_M4A1 && RANDOM_FLOAT(0, 1) <= .75f)
				AddEquipment(Equipment::M203Grenades);
		}		
	}

	auto pCurrentWeapon = GetCurrentWeapon();
	if (pCurrentWeapon)
	{		
		m_cClipSize = pCurrentWeapon->GetMaxClip();
		if (m_cClipSize > 0)
		{
			pCurrentWeapon->m_iClip = m_cClipSize;
		}
		if (pCurrentWeapon->GetWeaponId() == Weapon_Shotgun)
		{
			AddEquipment(Equipment::AmmoBuckshot);
		}
		else if (pCurrentWeapon->GetWeaponId() == Weapon_Mp5)
		{
			AddEquipment(Equipment::AmmoMp5Clips);
		}
		else if (pCurrentWeapon->GetWeaponId() == Weapon_M4A1)
		{
			AddEquipment(Equipment::AmmoM4Clips);
		}		
	}
	else
	{
		m_cClipSize = -1;
	}

	m_cAmmoLoaded = m_cClipSize;

	const auto headSelect = RANDOM_LONG(0, 99);
	if (headSelect < 5)
	{
		SetCurrentHead(HeadBodyPart::CommanderBlack);
		pev->skin = 1;
	}
	else if (headSelect < 15)
	{
		SetCurrentHead(HeadBodyPart::CommanderWhite);
		pev->skin = 0;
	}
	else
	{
		pev->skin = RANDOM_LONG(0, 100) < 80 ? 0 : 1;
		if (headSelect < 40)
			SetCurrentHead(HeadBodyPart::Balaclava);
		else
			SetCurrentHead(HeadBodyPart::GasMask);
	}

	CTalkMonster::g_talkWaitTime = 0;

	MonsterInit();

	if (!RANDOM_LONG(0, 3))
	{
		spawnable_item::SpawnOne(SpawnCategory::HumanMilitary, pev->origin);
	}
}

//=========================================================
// Precache - precaches all resources this monster needs
//=========================================================
void CHGrunt::Precache()
{
	PRECACHE_MODEL("models/hgrunt.mdl");

	PRECACHE_SOUND("hgrunt/gr_mgun1.wav");
	PRECACHE_SOUND("hgrunt/gr_mgun2.wav");

	PRECACHE_SOUND("hgrunt/gr_die1.wav");
	PRECACHE_SOUND("hgrunt/gr_die2.wav");
	PRECACHE_SOUND("hgrunt/gr_die3.wav");

	PRECACHE_SOUND("hgrunt/gr_pain1.wav");
	PRECACHE_SOUND("hgrunt/gr_pain2.wav");
	PRECACHE_SOUND("hgrunt/gr_pain3.wav");
	PRECACHE_SOUND("hgrunt/gr_pain4.wav");
	PRECACHE_SOUND("hgrunt/gr_pain5.wav");

	PRECACHE_SOUND("hgrunt/gr_reload1.wav");

	PRECACHE_SOUND("weapons/glauncher.wav");

	PRECACHE_SOUND("weapons/sbarrel1.wav");

	PRECACHE_SOUND("zombie/claw_miss2.wav"); // because we use the basemonster SWIPE animation event

	// get voice pitch
	m_voicePitch = 100 + RANDOM_LONG(-25, 25);
}

//=========================================================
// start task
//=========================================================
void CHGrunt::StartTask(Task_t* pTask)
{
	m_iTaskStatus = TASKSTATUS_RUNNING;

	switch (pTask->iTask)
	{
	case TASK_GRUNT_CHECK_FIRE:
		if (!NoFriendlyFire())
		{
			SetConditions(bits_COND_GRUNT_NOFIRE);
		}
		TaskComplete();
		break;

	case TASK_GRUNT_SPEAK_SENTENCE:
		SpeakSentence();
		TaskComplete();
		break;

	case TASK_WALK_PATH:
	case TASK_RUN_PATH:
		// grunt no longer assumes he is covered if he moves
		Forget(bits_MEMORY_INCOVER);
		CSquadMonster::StartTask(pTask);
		break;

	case TASK_RELOAD:
		m_IdealActivity = ACT_RELOAD;
		break;

	case TASK_GRUNT_FACE_TOSS_DIR:
		break;

	case TASK_FACE_IDEAL:
	case TASK_FACE_ENEMY:
		CSquadMonster::StartTask(pTask);
		if (pev->movetype == MOVETYPE_FLY)
		{
			m_IdealActivity = ACT_GLIDE;
		}
		break;

	default:
		CSquadMonster::StartTask(pTask);
		break;
	}
}

//=========================================================
// RunTask
//=========================================================
void CHGrunt::RunTask(Task_t* pTask)
{
	switch (pTask->iTask)
	{
	case TASK_GRUNT_FACE_TOSS_DIR:
		{
			// project a point along the toss vector and turn to face that point.
			MakeIdealYaw(pev->origin + m_vecTossVelocity * 64);
			ChangeYaw(pev->yaw_speed);

			if (FacingIdeal())
			{
				m_iTaskStatus = TASKSTATUS_COMPLETE;
			}
			break;
		}
	default:
		{
			CSquadMonster::RunTask(pTask);
			break;
		}
	}
}

//=========================================================
// PainSound
//=========================================================
void CHGrunt::PainSound()
{
	if (gpGlobals->time > m_flNextPainTime)
	{
#if 0
		if ( RANDOM_LONG(0, 99) < 5 )
		{
			// pain sentences are rare
			if (FOkToSpeak())
			{
				SENTENCEG_PlayRndSz(ENT(pev), "HG_PAIN", HGRUNT_SENTENCE_VOLUME, ATTN_NORM, 0, PITCH_NORM);
				JustSpoke();
				return;
			}
		}
#endif
		switch (RANDOM_LONG(0, 6))
		{
		case 0:
			EMIT_SOUND(ENT(pev), CHAN_VOICE, "hgrunt/gr_pain3.wav", 1, ATTN_NORM);
			break;
		case 1:
			EMIT_SOUND(ENT(pev), CHAN_VOICE, "hgrunt/gr_pain4.wav", 1, ATTN_NORM);
			break;
		case 2:
			EMIT_SOUND(ENT(pev), CHAN_VOICE, "hgrunt/gr_pain5.wav", 1, ATTN_NORM);
			break;
		case 3:
			EMIT_SOUND(ENT(pev), CHAN_VOICE, "hgrunt/gr_pain1.wav", 1, ATTN_NORM);
			break;
		case 4:
			EMIT_SOUND(ENT(pev), CHAN_VOICE, "hgrunt/gr_pain2.wav", 1, ATTN_NORM);
			break;
		}

		m_flNextPainTime = gpGlobals->time + 1;
	}
}

//=========================================================
// DeathSound 
//=========================================================
void CHGrunt::DeathSound()
{
	switch (RANDOM_LONG(0, 2))
	{
	case 0:
		EMIT_SOUND(ENT(pev), CHAN_VOICE, "hgrunt/gr_die1.wav", 1, ATTN_IDLE);
		break;
	case 1:
		EMIT_SOUND(ENT(pev), CHAN_VOICE, "hgrunt/gr_die2.wav", 1, ATTN_IDLE);
		break;
	case 2:
		EMIT_SOUND(ENT(pev), CHAN_VOICE, "hgrunt/gr_die3.wav", 1, ATTN_IDLE);
		break;
	}
}

//=========================================================
// AI Schedules Specific to this monster
//=========================================================

//=========================================================
// GruntFail
//=========================================================
Task_t tlGruntFail[] =
{
	{TASK_STOP_MOVING, 0},
	{TASK_SET_ACTIVITY, (float)ACT_IDLE},
	{TASK_WAIT, (float)2},
	{TASK_WAIT_PVS, (float)0},
};

Schedule_t slGruntFail[] =
{
	{
		tlGruntFail,
		ARRAYSIZE(tlGruntFail),
		bits_COND_CAN_ATTACK | bits_COND_HEAR_SOUND,
		0,
		"Grunt Fail"
	},
};

//=========================================================
// Grunt Combat Fail
//=========================================================
Task_t tlGruntCombatFail[] =
{
	{TASK_STOP_MOVING, 0},
	{TASK_SET_ACTIVITY, (float)ACT_IDLE},
	{TASK_WAIT_FACE_ENEMY, (float)2},
	{TASK_WAIT_PVS, (float)0},
};

Schedule_t slGruntCombatFail[] =
{
	{
		tlGruntCombatFail,
		ARRAYSIZE(tlGruntCombatFail),
		bits_COND_CAN_ATTACK | bits_COND_HEAR_SOUND,
		0,
		"Grunt Combat Fail"
	},
};

//=========================================================
// Victory dance!
//=========================================================
Task_t tlGruntVictoryDance[] =
{
	{TASK_STOP_MOVING, (float)0},
	{TASK_FACE_ENEMY, (float)0},
	{TASK_GET_PATH_TO_ENEMY_CORPSE, (float)0},
	{TASK_SET_ACTIVITY, ACT_COMBAT_IDLE},
	{TASK_WAIT_RANDOM, (float)5.0},
	{TASK_WALK_PATH, (float)0},
	{TASK_WAIT_FOR_MOVEMENT, (float)0},
	{TASK_FACE_ENEMY, (float)0},
	{TASK_PLAY_SEQUENCE, (float)ACT_VICTORY_DANCE},
};

Schedule_t slGruntVictoryDance[] =
{
	{
		tlGruntVictoryDance,
		ARRAYSIZE(tlGruntVictoryDance),
		bits_COND_NEW_ENEMY |
		bits_COND_LIGHT_DAMAGE |
		bits_COND_HEAVY_DAMAGE,
		0,
		"GruntVictoryDance"
	},
};

//=========================================================
// Establish line of fire - move to a position that allows
// the grunt to attack.
//=========================================================
Task_t tlGruntEstablishLineOfFire[] =
{
	{TASK_SET_FAIL_SCHEDULE, (float)SCHED_GRUNT_ELOF_FAIL},
	{TASK_GET_PATH_TO_ENEMY, (float)0},
	{TASK_GRUNT_SPEAK_SENTENCE, (float)0},
	{TASK_RUN_PATH, (float)0},
	{TASK_WAIT_FOR_MOVEMENT, (float)0},
};

Schedule_t slGruntEstablishLineOfFire[] =
{
	{
		tlGruntEstablishLineOfFire,
		ARRAYSIZE(tlGruntEstablishLineOfFire),
		bits_COND_NEW_ENEMY |
		bits_COND_ENEMY_DEAD |
		bits_COND_CAN_RANGE_ATTACK1 |
		bits_COND_CAN_MELEE_ATTACK1 |
		bits_COND_CAN_RANGE_ATTACK2 |
		bits_COND_CAN_MELEE_ATTACK2 |
		bits_COND_HEAR_SOUND,

		bits_SOUND_DANGER,
		"GruntEstablishLineOfFire"
	},
};

//=========================================================
// GruntFoundEnemy - grunt established sight with an enemy
// that was hiding from the squad.
//=========================================================
Task_t tlGruntFoundEnemy[] =
{
	{TASK_STOP_MOVING, 0},
	{TASK_FACE_ENEMY, (float)0},
	{TASK_PLAY_SEQUENCE_FACE_ENEMY, (float)ACT_SIGNAL1},
};

Schedule_t slGruntFoundEnemy[] =
{
	{
		tlGruntFoundEnemy,
		ARRAYSIZE(tlGruntFoundEnemy),
		bits_COND_HEAR_SOUND,

		bits_SOUND_DANGER,
		"GruntFoundEnemy"
	},
};

//=========================================================
// GruntCombatFace Schedule
//=========================================================
Task_t tlGruntCombatFace1[] =
{
	{TASK_STOP_MOVING, 0},
	{TASK_SET_ACTIVITY, (float)ACT_IDLE},
	{TASK_FACE_ENEMY, (float)0},
	{TASK_WAIT, (float)1.5},
	{TASK_SET_SCHEDULE, (float)SCHED_GRUNT_SWEEP},
};

Schedule_t slGruntCombatFace[] =
{
	{
		tlGruntCombatFace1,
		ARRAYSIZE(tlGruntCombatFace1),
		bits_COND_NEW_ENEMY |
		bits_COND_ENEMY_DEAD |
		bits_COND_CAN_RANGE_ATTACK1 |
		bits_COND_CAN_RANGE_ATTACK2,
		0,
		"Combat Face"
	},
};

//=========================================================
// Suppressing fire - don't stop shooting until the clip is
// empty or grunt gets hurt.
//=========================================================
Task_t tlGruntSignalSuppress[] =
{
	{TASK_STOP_MOVING, 0},
	{TASK_FACE_IDEAL, (float)0},
	{TASK_PLAY_SEQUENCE_FACE_ENEMY, (float)ACT_SIGNAL2},
	{TASK_FACE_ENEMY, (float)0},
	{TASK_GRUNT_CHECK_FIRE, (float)0},
	{TASK_RANGE_ATTACK1, (float)0},
	{TASK_FACE_ENEMY, (float)0},
	{TASK_GRUNT_CHECK_FIRE, (float)0},
	{TASK_RANGE_ATTACK1, (float)0},
	{TASK_FACE_ENEMY, (float)0},
	{TASK_GRUNT_CHECK_FIRE, (float)0},
	{TASK_RANGE_ATTACK1, (float)0},
	{TASK_FACE_ENEMY, (float)0},
	{TASK_GRUNT_CHECK_FIRE, (float)0},
	{TASK_RANGE_ATTACK1, (float)0},
	{TASK_FACE_ENEMY, (float)0},
	{TASK_GRUNT_CHECK_FIRE, (float)0},
	{TASK_RANGE_ATTACK1, (float)0},
};

Schedule_t slGruntSignalSuppress[] =
{
	{
		tlGruntSignalSuppress,
		ARRAYSIZE(tlGruntSignalSuppress),
		bits_COND_ENEMY_DEAD |
		bits_COND_LIGHT_DAMAGE |
		bits_COND_HEAVY_DAMAGE |
		bits_COND_HEAR_SOUND |
		bits_COND_GRUNT_NOFIRE |
		bits_COND_NO_AMMO_LOADED,

		bits_SOUND_DANGER,
		"SignalSuppress"
	},
};

Task_t tlGruntSuppress[] =
{
	{TASK_STOP_MOVING, 0},
	{TASK_FACE_ENEMY, (float)0},
	{TASK_GRUNT_CHECK_FIRE, (float)0},
	{TASK_RANGE_ATTACK1, (float)0},
	{TASK_FACE_ENEMY, (float)0},
	{TASK_GRUNT_CHECK_FIRE, (float)0},
	{TASK_RANGE_ATTACK1, (float)0},
	{TASK_FACE_ENEMY, (float)0},
	{TASK_GRUNT_CHECK_FIRE, (float)0},
	{TASK_RANGE_ATTACK1, (float)0},
	{TASK_FACE_ENEMY, (float)0},
	{TASK_GRUNT_CHECK_FIRE, (float)0},
	{TASK_RANGE_ATTACK1, (float)0},
	{TASK_FACE_ENEMY, (float)0},
	{TASK_GRUNT_CHECK_FIRE, (float)0},
	{TASK_RANGE_ATTACK1, (float)0},
};

Schedule_t slGruntSuppress[] =
{
	{
		tlGruntSuppress,
		ARRAYSIZE(tlGruntSuppress),
		bits_COND_ENEMY_DEAD |
		bits_COND_LIGHT_DAMAGE |
		bits_COND_HEAVY_DAMAGE |
		bits_COND_HEAR_SOUND |
		bits_COND_GRUNT_NOFIRE |
		bits_COND_NO_AMMO_LOADED,

		bits_SOUND_DANGER,
		"Suppress"
	},
};


//=========================================================
// grunt wait in cover - we don't allow danger or the ability
// to attack to break a grunt's run to cover schedule, but
// when a grunt is in cover, we do want them to attack if they can.
//=========================================================
Task_t tlGruntWaitInCover[] =
{
	{TASK_STOP_MOVING, (float)0},
	{TASK_SET_ACTIVITY, (float)ACT_IDLE},
	{TASK_WAIT_FACE_ENEMY, (float)1},
};

Schedule_t slGruntWaitInCover[] =
{
	{
		tlGruntWaitInCover,
		ARRAYSIZE(tlGruntWaitInCover),
		bits_COND_NEW_ENEMY |
		bits_COND_HEAR_SOUND |
		bits_COND_CAN_RANGE_ATTACK1 |
		bits_COND_CAN_RANGE_ATTACK2 |
		bits_COND_CAN_MELEE_ATTACK1 |
		bits_COND_CAN_MELEE_ATTACK2,

		bits_SOUND_DANGER,
		"GruntWaitInCover"
	},
};

//=========================================================
// run to cover.
// !!!BUGBUG - set a decent fail schedule here.
//=========================================================
Task_t tlGruntTakeCover1[] =
{
	{TASK_STOP_MOVING, (float)0},
	{TASK_SET_FAIL_SCHEDULE, (float)SCHED_GRUNT_TAKECOVER_FAILED},
	{TASK_WAIT, (float)0.2},
	{TASK_FIND_COVER_FROM_ENEMY, (float)0},
	{TASK_GRUNT_SPEAK_SENTENCE, (float)0},
	{TASK_RUN_PATH, (float)0},
	{TASK_WAIT_FOR_MOVEMENT, (float)0},
	{TASK_REMEMBER, (float)bits_MEMORY_INCOVER},
	{TASK_SET_SCHEDULE, (float)SCHED_GRUNT_WAIT_FACE_ENEMY},
};

Schedule_t slGruntTakeCover[] =
{
	{
		tlGruntTakeCover1,
		ARRAYSIZE(tlGruntTakeCover1),
		0,
		0,
		"TakeCover"
	},
};

//=========================================================
// drop grenade then run to cover.
//=========================================================
Task_t tlGruntGrenadeCover1[] =
{
	{TASK_STOP_MOVING, (float)0},
	{TASK_FIND_COVER_FROM_ENEMY, (float)99},
	{TASK_FIND_FAR_NODE_COVER_FROM_ENEMY, (float)384},
	{TASK_PLAY_SEQUENCE, (float)ACT_SPECIAL_ATTACK1},
	{TASK_CLEAR_MOVE_WAIT, (float)0},
	{TASK_RUN_PATH, (float)0},
	{TASK_WAIT_FOR_MOVEMENT, (float)0},
	{TASK_SET_SCHEDULE, (float)SCHED_GRUNT_WAIT_FACE_ENEMY},
};

Schedule_t slGruntGrenadeCover[] =
{
	{
		tlGruntGrenadeCover1,
		ARRAYSIZE(tlGruntGrenadeCover1),
		0,
		0,
		"GrenadeCover"
	},
};


//=========================================================
// drop grenade then run to cover.
//=========================================================
Task_t tlGruntTossGrenadeCover1[] =
{
	{TASK_FACE_ENEMY, (float)0},
	{TASK_RANGE_ATTACK2, (float)0},
	{TASK_SET_SCHEDULE, (float)SCHED_TAKE_COVER_FROM_ENEMY},
};

Schedule_t slGruntTossGrenadeCover[] =
{
	{
		tlGruntTossGrenadeCover1,
		ARRAYSIZE(tlGruntTossGrenadeCover1),
		0,
		0,
		"TossGrenadeCover"
	},
};

//=========================================================
// hide from the loudest sound source (to run from grenade)
//=========================================================
Task_t tlGruntTakeCoverFromBestSound[] =
{
	{TASK_SET_FAIL_SCHEDULE, (float)SCHED_COWER}, // duck and cover if cannot move from explosion
	{TASK_STOP_MOVING, (float)0},
	{TASK_FIND_COVER_FROM_BEST_SOUND, (float)0},
	{TASK_RUN_PATH, (float)0},
	{TASK_WAIT_FOR_MOVEMENT, (float)0},
	{TASK_REMEMBER, (float)bits_MEMORY_INCOVER},
	{TASK_TURN_LEFT, (float)179},
};

Schedule_t slGruntTakeCoverFromBestSound[] =
{
	{
		tlGruntTakeCoverFromBestSound,
		ARRAYSIZE(tlGruntTakeCoverFromBestSound),
		0,
		0,
		"GruntTakeCoverFromBestSound"
	},
};

//=========================================================
// Grunt reload schedule
//=========================================================
Task_t tlGruntHideReload[] =
{
	{TASK_STOP_MOVING, (float)0},
	{TASK_SET_FAIL_SCHEDULE, (float)SCHED_RELOAD},
	{TASK_FIND_COVER_FROM_ENEMY, (float)0},
	{TASK_RUN_PATH, (float)0},
	{TASK_WAIT_FOR_MOVEMENT, (float)0},
	{TASK_REMEMBER, (float)bits_MEMORY_INCOVER},
	{TASK_FACE_ENEMY, (float)0},
	{TASK_PLAY_SEQUENCE, (float)ACT_RELOAD},
};

Schedule_t slGruntHideReload[] =
{
	{
		tlGruntHideReload,
		ARRAYSIZE(tlGruntHideReload),
		bits_COND_HEAVY_DAMAGE |
		bits_COND_HEAR_SOUND,

		bits_SOUND_DANGER,
		"GruntHideReload"
	}
};

//=========================================================
// Do a turning sweep of the area
//=========================================================
Task_t tlGruntSweep[] =
{
	{TASK_TURN_LEFT, (float)179},
	{TASK_WAIT, (float)1},
	{TASK_TURN_LEFT, (float)179},
	{TASK_WAIT, (float)1},
};

Schedule_t slGruntSweep[] =
{
	{
		tlGruntSweep,
		ARRAYSIZE(tlGruntSweep),

		bits_COND_NEW_ENEMY |
		bits_COND_LIGHT_DAMAGE |
		bits_COND_HEAVY_DAMAGE |
		bits_COND_CAN_RANGE_ATTACK1 |
		bits_COND_CAN_RANGE_ATTACK2 |
		bits_COND_HEAR_SOUND,

		bits_SOUND_WORLD | // sound flags
		bits_SOUND_DANGER |
		bits_SOUND_PLAYER,

		"Grunt Sweep"
	},
};

//=========================================================
// primary range attack. Overriden because base class stops attacking when the enemy is occluded.
// grunt's grenade toss requires the enemy be occluded.
//=========================================================
Task_t tlGruntRangeAttack1A[] =
{
	{TASK_STOP_MOVING, (float)0},
	{TASK_PLAY_SEQUENCE_FACE_ENEMY, (float)ACT_CROUCH},
	{TASK_GRUNT_CHECK_FIRE, (float)0},
	{TASK_RANGE_ATTACK1, (float)0},
	{TASK_FACE_ENEMY, (float)0},
	{TASK_GRUNT_CHECK_FIRE, (float)0},
	{TASK_RANGE_ATTACK1, (float)0},
	{TASK_FACE_ENEMY, (float)0},
	{TASK_GRUNT_CHECK_FIRE, (float)0},
	{TASK_RANGE_ATTACK1, (float)0},
	{TASK_FACE_ENEMY, (float)0},
	{TASK_GRUNT_CHECK_FIRE, (float)0},
	{TASK_RANGE_ATTACK1, (float)0},
};

Schedule_t slGruntRangeAttack1A[] =
{
	{
		tlGruntRangeAttack1A,
		ARRAYSIZE(tlGruntRangeAttack1A),
		bits_COND_NEW_ENEMY |
		bits_COND_ENEMY_DEAD |
		bits_COND_HEAVY_DAMAGE |
		bits_COND_ENEMY_OCCLUDED |
		bits_COND_HEAR_SOUND |
		bits_COND_GRUNT_NOFIRE |
		bits_COND_NO_AMMO_LOADED,

		bits_SOUND_DANGER,
		"Range Attack1A"
	},
};


//=========================================================
// primary range attack. Overriden because base class stops attacking when the enemy is occluded.
// grunt's grenade toss requires the enemy be occluded.
//=========================================================
Task_t tlGruntRangeAttack1B[] =
{
	{TASK_STOP_MOVING, (float)0},
	{TASK_PLAY_SEQUENCE_FACE_ENEMY, (float)ACT_IDLE_ANGRY},
	{TASK_GRUNT_CHECK_FIRE, (float)0},
	{TASK_RANGE_ATTACK1, (float)0},
	{TASK_FACE_ENEMY, (float)0},
	{TASK_GRUNT_CHECK_FIRE, (float)0},
	{TASK_RANGE_ATTACK1, (float)0},
	{TASK_FACE_ENEMY, (float)0},
	{TASK_GRUNT_CHECK_FIRE, (float)0},
	{TASK_RANGE_ATTACK1, (float)0},
	{TASK_FACE_ENEMY, (float)0},
	{TASK_GRUNT_CHECK_FIRE, (float)0},
	{TASK_RANGE_ATTACK1, (float)0},
};

Schedule_t slGruntRangeAttack1B[] =
{
	{
		tlGruntRangeAttack1B,
		ARRAYSIZE(tlGruntRangeAttack1B),
		bits_COND_NEW_ENEMY |
		bits_COND_ENEMY_DEAD |
		bits_COND_HEAVY_DAMAGE |
		bits_COND_ENEMY_OCCLUDED |
		bits_COND_NO_AMMO_LOADED |
		bits_COND_GRUNT_NOFIRE |
		bits_COND_HEAR_SOUND,

		bits_SOUND_DANGER,
		"Range Attack1B"
	},
};

//=========================================================
// secondary range attack. Overriden because base class stops attacking when the enemy is occluded.
// grunt's grenade toss requires the enemy be occluded.
//=========================================================
Task_t tlGruntRangeAttack2[] =
{
	{TASK_STOP_MOVING, (float)0},
	{TASK_GRUNT_FACE_TOSS_DIR, (float)0},
	{TASK_PLAY_SEQUENCE, (float)ACT_RANGE_ATTACK2},
	{TASK_SET_SCHEDULE, (float)SCHED_GRUNT_WAIT_FACE_ENEMY}, // don't run immediately after throwing grenade.
};

Schedule_t slGruntRangeAttack2[] =
{
	{
		tlGruntRangeAttack2,
		ARRAYSIZE(tlGruntRangeAttack2),
		0,
		0,
		"RangeAttack2"
	},
};


//=========================================================
// repel 
//=========================================================
Task_t tlGruntRepel[] =
{
	{TASK_STOP_MOVING, (float)0},
	{TASK_FACE_IDEAL, (float)0},
	{TASK_PLAY_SEQUENCE, (float)ACT_GLIDE},
};

Schedule_t slGruntRepel[] =
{
	{
		tlGruntRepel,
		ARRAYSIZE(tlGruntRepel),
		bits_COND_SEE_ENEMY |
		bits_COND_NEW_ENEMY |
		bits_COND_LIGHT_DAMAGE |
		bits_COND_HEAVY_DAMAGE |
		bits_COND_HEAR_SOUND,

		bits_SOUND_DANGER |
		bits_SOUND_COMBAT |
		bits_SOUND_PLAYER,
		"Repel"
	},
};


//=========================================================
// repel 
//=========================================================
Task_t tlGruntRepelAttack[] =
{
	{TASK_STOP_MOVING, (float)0},
	{TASK_FACE_ENEMY, (float)0},
	{TASK_PLAY_SEQUENCE, (float)ACT_FLY},
};

Schedule_t slGruntRepelAttack[] =
{
	{
		tlGruntRepelAttack,
		ARRAYSIZE(tlGruntRepelAttack),
		bits_COND_ENEMY_OCCLUDED,
		0,
		"Repel Attack"
	},
};

//=========================================================
// repel land
//=========================================================
Task_t tlGruntRepelLand[] =
{
	{TASK_STOP_MOVING, (float)0},
	{TASK_PLAY_SEQUENCE, (float)ACT_LAND},
	{TASK_GET_PATH_TO_LASTPOSITION, (float)0},
	{TASK_RUN_PATH, (float)0},
	{TASK_WAIT_FOR_MOVEMENT, (float)0},
	{TASK_CLEAR_LASTPOSITION, (float)0},
};

Schedule_t slGruntRepelLand[] =
{
	{
		tlGruntRepelLand,
		ARRAYSIZE(tlGruntRepelLand),
		bits_COND_SEE_ENEMY |
		bits_COND_NEW_ENEMY |
		bits_COND_LIGHT_DAMAGE |
		bits_COND_HEAVY_DAMAGE |
		bits_COND_HEAR_SOUND,

		bits_SOUND_DANGER |
		bits_SOUND_COMBAT |
		bits_SOUND_PLAYER,
		"Repel Land"
	},
};


DEFINE_CUSTOM_SCHEDULES(CHGrunt)
	{
		slGruntFail,
		slGruntCombatFail,
		slGruntVictoryDance,
		slGruntEstablishLineOfFire,
		slGruntFoundEnemy,
		slGruntCombatFace,
		slGruntSignalSuppress,
		slGruntSuppress,
		slGruntWaitInCover,
		slGruntTakeCover,
		slGruntGrenadeCover,
		slGruntTossGrenadeCover,
		slGruntTakeCoverFromBestSound,
		slGruntHideReload,
		slGruntSweep,
		slGruntRangeAttack1A,
		slGruntRangeAttack1B,
		slGruntRangeAttack2,
		slGruntRepel,
		slGruntRepelAttack,
		slGruntRepelLand,
	};

IMPLEMENT_CUSTOM_SCHEDULES(CHGrunt, CSquadMonster);

//=========================================================
// SetActivity 
//=========================================================
void CHGrunt::SetActivity(Activity NewActivity)
{
	int iSequence = ACTIVITY_NOT_AVAILABLE;
	void* pmodel = GET_MODEL_PTR(ENT(pev));
	auto pWeapon = GetCurrentWeapon();

	switch (NewActivity)
	{
	case ACT_RANGE_ATTACK1:
		if (!pWeapon)
		{
			//!!! TODO: alert?
			iSequence = LookupSequence("trackwave");
		}
		else {
			const auto suffix = pWeapon->GetWeaponInfo().npcAnimationSuffix;
			char formatTarget[256];
			ZeroMemory(formatTarget, 256);
			sprintf(formatTarget, m_fStanding ? "standing_%s" : "crouching_%s", suffix);
			iSequence = LookupSequence(formatTarget);

			// Unsupported animation suffix, should use something else instead
			if (iSequence < 0)
				iSequence = LookupSequence(m_fStanding ? "standing_shotgun" : "crouching_shotgun");
		}
		break;
	case ACT_RANGE_ATTACK2:
		// grunt is going to a secondary long range attack. This may be a thrown 
		// grenade or fired grenade, we must determine which and pick proper sequence
		if (HasEquipment(Equipment::HandGrenade))
		{
			// get toss anim
			iSequence = LookupSequence("throwgrenade");
		}
		else
		{
			// get launch anim
			iSequence = LookupSequence("launchgrenade");
		}
		break;
	case ACT_RUN:
		if (pev->health <= HGRUNT_LIMP_HEALTH)
		{
			// limp!
			iSequence = LookupActivity(ACT_RUN_HURT);
		}
		else
		{
			iSequence = LookupActivity(NewActivity);
		}
		break;
	case ACT_WALK:
		if (pev->health <= HGRUNT_LIMP_HEALTH)
		{
			// limp!
			iSequence = LookupActivity(ACT_WALK_HURT);
		}
		else
		{
			iSequence = LookupActivity(NewActivity);
		}
		break;
	case ACT_IDLE:
		if (m_MonsterState == MONSTERSTATE_COMBAT)
		{
			NewActivity = ACT_IDLE_ANGRY;
		}
		iSequence = LookupActivity(NewActivity);
		break;
	default:
		iSequence = LookupActivity(NewActivity);
		break;
	}

	m_Activity = NewActivity; // Go ahead and set this so it doesn't keep trying when the anim is not present

	// Set to the desired anim, or default anim if the desired is not present
	if (iSequence > ACTIVITY_NOT_AVAILABLE)
	{
		if (pev->sequence != iSequence || !m_fSequenceLoops)
		{
			pev->frame = 0;
		}

		pev->sequence = iSequence; // Set to the reset anim (if it's there)
		ResetSequenceInfo();
		SetYawSpeed();
	}
	else
	{
		// Not available try to get default anim
		ALERT(at_console, "%s has no sequence for act:%d\n", STRING(pev->classname), NewActivity);
		pev->sequence = 0; // Set to the reset anim (if it's there)
	}
}

//=========================================================
// Get Schedule!
//=========================================================
Schedule_t* CHGrunt::GetSchedule()
{
	// clear old sentence
	m_iSentence = HGRUNT_SENT_NONE;

	// flying? If PRONE, barnacle has me. IF not, it's assumed I am rapelling. 
	if (pev->movetype == MOVETYPE_FLY && m_MonsterState != MONSTERSTATE_PRONE)
	{
		if (pev->flags & FL_ONGROUND)
		{
			// just landed
			pev->movetype = MOVETYPE_STEP;
			return GetScheduleOfType(SCHED_GRUNT_REPEL_LAND);
		}
		// repel down a rope, 
		if (m_MonsterState == MONSTERSTATE_COMBAT)
			return GetScheduleOfType(SCHED_GRUNT_REPEL_ATTACK);
		return GetScheduleOfType(SCHED_GRUNT_REPEL);
	}

	// grunts place HIGH priority on running away from danger sounds.
	if (HasConditions(bits_COND_HEAR_SOUND))
	{
		auto pSound = PBestSound();

		ASSERT(pSound != NULL);
		if (pSound)
		{
			if (pSound->m_iType & bits_SOUND_DANGER)
			{
				// dangerous sound nearby!

				//!!!KELLY - currently, this is the grunt's signal that a grenade has landed nearby,
				// and the grunt should find cover from the blast
				// good place for "SHIT!" or some other colorful verbal indicator of dismay.
				// It's not safe to play a verbal order here "Scatter", etc cause 
				// this may only affect a single individual in a squad. 

				if (FOkToSpeak())
				{
					SENTENCEG_PlayRndSz(ENT(pev), "HG_GREN", HGRUNT_SENTENCE_VOLUME, GRUNT_ATTN, 0, m_voicePitch);
					JustSpoke();
				}
				return GetScheduleOfType(SCHED_TAKE_COVER_FROM_BEST_SOUND);
			}

			if (!HasConditions(bits_COND_SEE_ENEMY) && (pSound->m_iType & (bits_SOUND_PLAYER | bits_SOUND_COMBAT)))
			{
				MakeIdealYaw(pSound->m_vecOrigin);
			}
		}
	}
	switch (m_MonsterState)
	{
	case MONSTERSTATE_COMBAT:
		{
			// dead enemy
			if (HasConditions(bits_COND_ENEMY_DEAD))
			{
				// call base class, all code to handle dead enemies is centralized there.
				return CBaseMonster::GetSchedule();
			}

			// new enemy
			if (HasConditions(bits_COND_NEW_ENEMY))
			{
				if (InSquad())
				{
					MySquadLeader()->m_fEnemyEluded = false;

					if (!IsLeader() && RANDOM_LONG(0, 99) > 75)
					{
						return GetScheduleOfType(SCHED_TAKE_COVER_FROM_ENEMY);
					}

					if (IsLeader())
					{
						//!!!KELLY - the leader of a squad of grunts has just seen the player or a 
						// monster and has made it the squad's enemy. You
						// can check pev->flags for FL_CLIENT to determine whether this is the player
						// or a monster. He's going to immediately start
						// firing, though. If you'd like, we can make an alternate "first sight" 
						// schedule where the leader plays a handsign anim
						// that gives us enough time to hear a short sentence or spoken command
						// before he starts pluggin away.
						if (FOkToSpeak()) // && RANDOM_LONG(0,1))
						{
							if ((m_hEnemy != nullptr) && m_hEnemy->IsPlayer())
								// player
								SENTENCEG_PlayRndSz(ENT(pev), "HG_ALERT", HGRUNT_SENTENCE_VOLUME, GRUNT_ATTN, 0,
								                    m_voicePitch);
							else if ((m_hEnemy != nullptr) &&
								(m_hEnemy->Classify() != CLASS_PLAYER_ALLY) &&
								(m_hEnemy->Classify() != CLASS_HUMAN_PASSIVE) &&
								(m_hEnemy->Classify() != CLASS_MACHINE))
								// monster
								SENTENCEG_PlayRndSz(ENT(pev), "HG_MONST", HGRUNT_SENTENCE_VOLUME, GRUNT_ATTN, 0,
								                    m_voicePitch);

							JustSpoke();
						}
					}


					if (HasConditions(bits_COND_CAN_RANGE_ATTACK1))
					{
						return GetScheduleOfType(SCHED_GRUNT_SUPPRESS);
					}

					return GetScheduleOfType(SCHED_GRUNT_ESTABLISH_LINE_OF_FIRE);
				}
			}
				// no ammo
			else if (HasConditions(bits_COND_NO_AMMO_LOADED))
			{
				//!!!KELLY - this individual just realized he's out of bullet ammo. 
				// He's going to try to find cover to run to and reload, but rarely, if 
				// none is available, he'll drop and reload in the open here. 
				return GetScheduleOfType(SCHED_GRUNT_COVER_AND_RELOAD);
			}

				// damaged just a little
			else if (HasConditions(bits_COND_LIGHT_DAMAGE))
			{
				// if hurt:
				// 90% chance of taking cover
				// 10% chance of flinch.
				int iPercent = RANDOM_LONG(0, 99);

				if (iPercent <= 90 && m_hEnemy != nullptr)
				{
					// only try to take cover if we actually have an enemy!

					//!!!KELLY - this grunt was hit and is going to run to cover.
					if (FOkToSpeak()) // && RANDOM_LONG(0,1))
					{
						//SENTENCEG_PlayRndSz( ENT(pev), "HG_COVER", HGRUNT_SENTENCE_VOLUME, GRUNT_ATTN, 0, m_voicePitch);
						m_iSentence = HGRUNT_SENT_COVER;
						//JustSpoke();
					}
					return GetScheduleOfType(SCHED_TAKE_COVER_FROM_ENEMY);
				}

				return GetScheduleOfType(SCHED_SMALL_FLINCH);
			}
				// can kick
			else if (HasConditions(bits_COND_CAN_MELEE_ATTACK1))
			{
				return GetScheduleOfType(SCHED_MELEE_ATTACK1);
			}
				// can grenade launch

			else if (HasEquipment(Equipment::M203Grenades) && HasConditions(bits_COND_CAN_RANGE_ATTACK2) && OccupySlot(
				bits_SLOTS_HGRUNT_GRENADE))
			{
				// shoot a grenade if you can
				return GetScheduleOfType(SCHED_RANGE_ATTACK2);
			}
				// can shoot
			else if (HasConditions(bits_COND_CAN_RANGE_ATTACK1))
			{
				if (InSquad())
				{
					// if the enemy has eluded the squad and a squad member has just located the enemy
					// and the enemy does not see the squad member, issue a call to the squad to waste a 
					// little time and give the player a chance to turn.
					if (MySquadLeader()->m_fEnemyEluded && !HasConditions(bits_COND_ENEMY_FACING_ME))
					{
						MySquadLeader()->m_fEnemyEluded = false;
						return GetScheduleOfType(SCHED_GRUNT_FOUND_ENEMY);
					}
				}

				if (OccupySlot(bits_SLOTS_HGRUNT_ENGAGE))
				{
					// try to take an available ENGAGE slot
					return GetScheduleOfType(SCHED_RANGE_ATTACK1);
				}

				if (HasConditions(bits_COND_CAN_RANGE_ATTACK2) && OccupySlot(bits_SLOTS_HGRUNT_GRENADE))
				{
					// throw a grenade if can and no engage slots are available
					return GetScheduleOfType(SCHED_RANGE_ATTACK2);
				}

				// hide!
				return GetScheduleOfType(SCHED_TAKE_COVER_FROM_ENEMY);
			}
				// can't see enemy
			else if (HasConditions(bits_COND_ENEMY_OCCLUDED))
			{
				if (HasConditions(bits_COND_CAN_RANGE_ATTACK2) && OccupySlot(bits_SLOTS_HGRUNT_GRENADE))
				{
					//!!!KELLY - this grunt is about to throw or fire a grenade at the player. Great place for "fire in the hole"  "frag out" etc
					if (FOkToSpeak())
					{
						SENTENCEG_PlayRndSz(ENT(pev), "HG_THROW", HGRUNT_SENTENCE_VOLUME, GRUNT_ATTN, 0, m_voicePitch);
						JustSpoke();
					}
					return GetScheduleOfType(SCHED_RANGE_ATTACK2);
				}

				if (OccupySlot(bits_SLOTS_HGRUNT_ENGAGE))
				{
					//!!!KELLY - grunt cannot see the enemy and has just decided to 
					// charge the enemy's position. 
					if (FOkToSpeak()) // && RANDOM_LONG(0,1))
					{
						//SENTENCEG_PlayRndSz( ENT(pev), "HG_CHARGE", HGRUNT_SENTENCE_VOLUME, GRUNT_ATTN, 0, m_voicePitch);
						m_iSentence = HGRUNT_SENT_CHARGE;
						//JustSpoke();
					}

					return GetScheduleOfType(SCHED_GRUNT_ESTABLISH_LINE_OF_FIRE);
				}

				//!!!KELLY - grunt is going to stay put for a couple seconds to see if
				// the enemy wanders back out into the open, or approaches the
				// grunt's covered position. Good place for a taunt, I guess?
				if (FOkToSpeak() && RANDOM_LONG(0, 1))
				{
					SENTENCEG_PlayRndSz(ENT(pev), "HG_TAUNT", HGRUNT_SENTENCE_VOLUME, GRUNT_ATTN, 0, m_voicePitch);
					JustSpoke();
				}
				return GetScheduleOfType(SCHED_STANDOFF);
			}

			if (HasConditions(bits_COND_SEE_ENEMY) && !HasConditions(bits_COND_CAN_RANGE_ATTACK1))
			{
				return GetScheduleOfType(SCHED_GRUNT_ESTABLISH_LINE_OF_FIRE);
			}
		}
	}

	// no special cases here, call the base class
	return CSquadMonster::GetSchedule();
}

//=========================================================
//=========================================================
Schedule_t* CHGrunt::GetScheduleOfType(int Type)
{
	switch (Type)
	{
	case SCHED_TAKE_COVER_FROM_ENEMY:
		{
			if (InSquad())
			{
				if (g_iSkillLevel == SKILL_HARD && HasConditions(bits_COND_CAN_RANGE_ATTACK2) && OccupySlot(
					bits_SLOTS_HGRUNT_GRENADE))
				{
					if (FOkToSpeak())
					{
						SENTENCEG_PlayRndSz(ENT(pev), "HG_THROW", HGRUNT_SENTENCE_VOLUME, GRUNT_ATTN, 0, m_voicePitch);
						JustSpoke();
					}
					return slGruntTossGrenadeCover;
				}
				return &slGruntTakeCover[0];
			}
			if (RANDOM_LONG(0, 1))
			{
				return &slGruntTakeCover[0];
			}
			return &slGruntGrenadeCover[0];
		}
	case SCHED_TAKE_COVER_FROM_BEST_SOUND:
		{
			return &slGruntTakeCoverFromBestSound[0];
		}
	case SCHED_GRUNT_TAKECOVER_FAILED:
		{
			if (HasConditions(bits_COND_NO_AMMO_LOADED))
				return GetScheduleOfType(SCHED_RELOAD);
			
			if (HasConditions(bits_COND_CAN_RANGE_ATTACK1))
				return GetScheduleOfType(SCHED_RANGE_ATTACK1);

			return GetScheduleOfType(SCHED_FAIL);
		}
		break;
	case SCHED_GRUNT_ELOF_FAIL:
		{
			// human grunt is unable to move to a position that allows him to attack the enemy.
			return GetScheduleOfType(SCHED_TAKE_COVER_FROM_ENEMY);
		}
		break;
	case SCHED_GRUNT_ESTABLISH_LINE_OF_FIRE:
		{
			return &slGruntEstablishLineOfFire[0];
		}
		break;
	case SCHED_RANGE_ATTACK1:
		{
			// randomly stand or crouch
			if (RANDOM_LONG(0, 9) == 0)
				m_fStanding = RANDOM_LONG(0, 1);

			if (m_fStanding)
				return &slGruntRangeAttack1B[0];
			return &slGruntRangeAttack1A[0];
		}
	case SCHED_RANGE_ATTACK2:
		{
			return &slGruntRangeAttack2[0];
		}
	case SCHED_COMBAT_FACE:
		{
			return &slGruntCombatFace[0];
		}
	case SCHED_GRUNT_WAIT_FACE_ENEMY:
		{
			return &slGruntWaitInCover[0];
		}
	case SCHED_GRUNT_SWEEP:
		{
			return &slGruntSweep[0];
		}
	case SCHED_GRUNT_COVER_AND_RELOAD:
		{
			return &slGruntHideReload[0];
		}
	case SCHED_GRUNT_FOUND_ENEMY:
		{
			return &slGruntFoundEnemy[0];
		}
	case SCHED_VICTORY_DANCE:
		{
			if (InSquad())
			{
				if (!IsLeader())
				{
					return &slGruntFail[0];
				}
			}

			return &slGruntVictoryDance[0];
		}
	case SCHED_GRUNT_SUPPRESS:
		{
			if (m_hEnemy->IsPlayer() && m_fFirstEncounter)
			{
				m_fFirstEncounter = false;
				// after first encounter, leader won't issue handsigns anymore when he has a new enemy
				return &slGruntSignalSuppress[0];
			}
			return &slGruntSuppress[0];
		}
	case SCHED_FAIL:
		{
			if (m_hEnemy != nullptr)
			{
				// grunt has an enemy, so pick a different default fail schedule most likely to help recover.
				return &slGruntCombatFail[0];
			}

			return &slGruntFail[0];
		}
	case SCHED_GRUNT_REPEL:
		{
			if (pev->velocity.z > -128)
				pev->velocity.z -= 32;
			return &slGruntRepel[0];
		}
	case SCHED_GRUNT_REPEL_ATTACK:
		{
			if (pev->velocity.z > -128)
				pev->velocity.z -= 32;
			return &slGruntRepelAttack[0];
		}
	case SCHED_GRUNT_REPEL_LAND:
		{
			return &slGruntRepelLand[0];
		}
	default:
		{
			return CSquadMonster::GetScheduleOfType(Type);
		}
	}
}


//=========================================================
// CHGruntRepel - when triggered, spawns a monster_human_grunt
// repelling down a line.
//=========================================================

class CHGruntRepel : public CBaseMonster
{
public:
	void Spawn() override;
	void Precache() override;
	void EXPORT RepelUse(CBaseEntity* pActivator, CBaseEntity* pCaller, USE_TYPE useType, float value);
	int m_iSpriteTexture; // Don't save, precache
};

LINK_ENTITY_TO_CLASS(monster_grunt_repel, CHGruntRepel);

void CHGruntRepel::Spawn()
{
	Precache();
	pev->solid = SOLID_NOT;

	SetUse(&CHGruntRepel::RepelUse);
}

void CHGruntRepel::Precache()
{
	UTIL_PrecacheOther("monster_human_grunt");
	m_iSpriteTexture = PRECACHE_MODEL("sprites/rope.spr");
}

void CHGruntRepel::RepelUse(CBaseEntity* pActivator, CBaseEntity* pCaller, USE_TYPE useType, float value)
{
	TraceResult tr;
	UTIL_TraceLine(pev->origin, pev->origin + Vector(0, 0, -4096.0), dont_ignore_monsters, ENT(pev), &tr);
	/*
	if ( tr.pHit && Instance( tr.pHit )->pev->solid != SOLID_BSP) 
		return NULL;
	*/

	CBaseEntity* pEntity = Create("monster_human_grunt", pev->origin, pev->angles);
	CBaseMonster* pGrunt = pEntity->MyMonsterPointer();
	pGrunt->pev->movetype = MOVETYPE_FLY;
	pGrunt->pev->velocity = Vector(0, 0, RANDOM_FLOAT(-196, -128));
	pGrunt->SetActivity(ACT_GLIDE);
	// UNDONE: position?
	pGrunt->m_vecLastPosition = tr.vecEndPos;

	CBeam* pBeam = CBeam::BeamCreate("sprites/rope.spr", 10);
	pBeam->PointEntInit(pev->origin + Vector(0, 0, 112), pGrunt->entindex());
	pBeam->SetFlags(BEAM_FSOLID);
	pBeam->SetColor(255, 255, 255);
	pBeam->SetThink(&CBeam::SUB_Remove);
	pBeam->pev->nextthink = gpGlobals->time + -4096.0 * tr.flFraction / pGrunt->pev->velocity.z + 0.5;

	UTIL_Remove(this);
}


//=========================================================
// DEAD HGRUNT PROP
//=========================================================
class CDeadHGrunt : public CBaseMonster
{
public:
	void Spawn() override;
	int Classify() override { return CLASS_HUMAN_MILITARY; }

	void KeyValue(KeyValueData* pkvd) override;

	int m_iPose; // which sequence to display	-- temporary, don't need to save
	static const char* m_szPoses[3];
};

const char* CDeadHGrunt::m_szPoses[] = {"deadstomach", "deadside", "deadsitting"};

void CDeadHGrunt::KeyValue(KeyValueData* pkvd)
{
	if (FStrEq(pkvd->szKeyName, "pose"))
	{
		m_iPose = atoi(pkvd->szValue);
		pkvd->fHandled = true;
	}
	else
		CBaseMonster::KeyValue(pkvd);
}

LINK_ENTITY_TO_CLASS(monster_hgrunt_dead, CDeadHGrunt);

//=========================================================
// ********** DeadHGrunt SPAWN **********
//=========================================================
void CDeadHGrunt::Spawn()
{
	PRECACHE_MODEL("models/hgrunt.mdl");
	SET_MODEL(ENT(pev), "models/hgrunt.mdl");

	pev->effects = 0;
	pev->yaw_speed = 8;
	pev->sequence = 0;
	m_bloodColor = BLOOD_COLOR_RED;

	pev->sequence = LookupSequence(m_szPoses[m_iPose]);

	if (pev->sequence == -1)
	{
		ALERT(at_console, "Dead hgrunt with bad pose\n");
	}

	// Corpses have less health
	pev->health = 8;

	//TODO: undone (ml7)
	// map old bodies onto new bodies
	switch (pev->body)
	{
	case 0: // Grunt with Gun
		pev->body = 0;
		pev->skin = 0;
		SetBodygroup((int)BodyGroup::Heads, (int)HeadBodyPart::GasMask);
		//SetBodygroup((int)BodyGroup::Guns, (int)GunBodyPart::M4);
		break;
	case 1: // Commander with Gun
		pev->body = 0;
		pev->skin = 0;
		SetBodygroup((int)BodyGroup::Heads, (int)HeadBodyPart::CommanderWhite);
		//SetBodygroup((int)BodyGroup::Guns, (int)GunBodyPart::M4);
		break;
	case 2: // Grunt no Gun
		pev->body = 0;
		pev->skin = 0;
		SetBodygroup((int)BodyGroup::Heads, (int)HeadBodyPart::GasMask);
		//SetBodygroup((int)BodyGroup::Guns, (int)GunBodyPart::None);
		break;
	case 3: // Commander no Gun
		pev->body = 0;
		pev->skin = 0;
		SetBodygroup((int)BodyGroup::Heads, (int)HeadBodyPart::CommanderWhite);
		//SetBodygroup((int)BodyGroup::Guns, (int)GunBodyPart::None);
		break;
	}

	MonsterInitDead();
}
