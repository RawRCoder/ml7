#include "extdll.h"
#include "util.h"
#include "cbase.h"
#include "shell_types.h"

const ShellTypesRepository* ShellTypesRepository::instance = nullptr;

const ShellTypesRepository* ShellTypesRepository::Get()
{
	if (!instance)
	{
		auto e = new ShellTypesRepository();
		e->Initialize();
		instance = e;
	}
	return instance;
}


void ShellTypesRepository::Initialize()
{
	DefineNewType(Shell_9x19mm, "models/shell.mdl");
	DefineNewType(Shell_Buckshot, "models/shotgunshell.mdl", TE_BOUNCE_SHOTSHELL);
	DefineNewType(Shell_45acp, "models/shell_45acp.mdl"); 
	DefineNewType(Shell_357Magnum, "models/shell.mdl"); //TODO: separate model?
	DefineNewType(Shell_556x45mm, "models/shell_5.56mm.mdl");
}


const ShellType& ShellTypesRepository::GetShellTypeById(ShellTypeId id) const
{
	if (id < 0 || id >= MAX_SHELL_TYPE_ID)
		throw "unknown shell type";

	return types[id];
}

ShellType& ShellTypesRepository::DefineNewType(const ShellTypeId id, const char* modelName, int bounceSound)
{
	auto& item = types[id];
	item = ShellType();
	item.id = id;
	item.modelName = modelName;
	item.bounceSoundType = bounceSound;
	return item;
}


int ShellType::Precache() const
{
	return PRECACHE_MODEL(modelName);
}
