#pragma once

enum WeaponId
{
	Weapon_None = -1,

	Weapon_Crowbar = 1,
	Weapon_Glock17,
	Weapon_Python,
	Weapon_Mp5,
	Weapon_M249,
	Weapon_Crossbow,
	Weapon_Shotgun,
	Weapon_RocketLauncher,
	Weapon_TauCannon,
	Weapon_GluonGun,
	Weapon_HornetGun,
	Weapon_HandGrenade,
	Weapon_TripMine,
	Weapon_Satchel,
	Weapon_Snarks,
	Weapon_M4A1,
	Weapon_Usp45,

	MAX_WEAPON_ID = 32
};

enum AmmoTypeId : int
{
	Ammo_None = -1,

	Ammo_00Buckshot = 1,
	Ammo_9x19mm,
	Ammo_40x46mm_M381,
	Ammo_357Magnum,
	Ammo_Uranium,
	Ammo_Rockets,
	Ammo_CrossbowBolts,
	Ammo_TripMines,
	Ammo_Satchels,
	Ammo_HandGrenades,
	Ammo_Snarks,
	Ammo_Hornets,

	Ammo_45acp,
	Ammo_556x45mm_NATO,
	Ammo_762x51mm_NATO,
	Ammo_50BMG,
	Ammo_30x113mm,

	MAX_AMMO_TYPE_ID
};


enum ShellTypeId
{
	Shell_None = -1,

	Shell_9x19mm,
	Shell_45acp,
	Shell_357Magnum,
	Shell_556x45mm,
	Shell_Buckshot,

	//TODO: M203 40mm shells?
	//TODO: 7.62mm NATO
	//TODO: .45 ACP
	//TODO: .50 BMG
	//TODO: 30x113mm ??

	MAX_SHELL_TYPE_ID
};

struct ShellType
{
	ShellTypeId id;
	const char* modelName;
	int bounceSoundType;

	int Precache() const;
};

struct AmmoType
{
	AmmoTypeId id;
	const char* name;

	int maxCarry;
	int tracerFrequency = 0;
	int bulletCount = 1;

	float bulletCaliber;
	float bulletMass;
	float bulletVelocity;
	float armorPiercingK = 0.5f;
	float bulletMaxDistance = 4096;
	int bulletType = -1;

	float explosivesCharge = 0.0f;
	ShellTypeId shellTypeId = Shell_None;


	float GetShotEnergy() const;
	bool HasShell() const { return shellTypeId > Shell_None; }
	bool ExplodesOnHit() const { return explosivesCharge > 0; }
};

enum WeaponFlags : int
{
	WeaponFlags_None = 0,
	WeaponFlags_SelectOnEmpty = 1,
	WeaponFlags_NoAutoReload = 2,
	WeaponFlags_NoAutoSwitchEmpty = 4,
	WeaponFlags_LimitWorld = 8,
	WeaponFlags_Exhaustible = 16
};

struct WeaponInfo
{
	WeaponId id;
	const char* className;
	const char* npcAnimationSuffix = nullptr;
	WeaponFlags flags = static_cast<WeaponFlags>(0);

	AmmoTypeId ammoTypeId = Ammo_None;
	int maxClip = -1;

	AmmoTypeId secondaryAmmoTypeId = Ammo_None;
	int maxSecondaryClip = -1;

	int hudWeaponSlot = -1;
	int hudWeaponSlotPosition = -1;
	int autoselectWeight = -1;
	bool canFireUnderwater = false;

	bool aiIsMelee = false;
	float aiMaxRange = -1;
	float aiMinSafeRange = -1;

	float GetMaxEffectiveRange() const
	{
		if (aiIsMelee)
			return aiMaxRange > 0 ? aiMaxRange : 70;

		if (aiMaxRange > 0)
			return aiMaxRange;

		auto pAmmo = GetAmmoType();
		return pAmmo ? pAmmo->bulletMaxDistance : 4096;
	}

	bool CanBeAutoSelected() const { return autoselectWeight >= 0; }
	const AmmoType* GetAmmoType() const;
	const AmmoType* GetSecondaryAmmoType() const;
};
