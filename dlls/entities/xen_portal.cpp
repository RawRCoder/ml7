#include "extdll.h"
#include "util.h"
#include "cbase.h"
#include "monsters.h"
#include "weapons.h"
#include "nodes.h"
#include "effects.h"
#include "xen_portal.h"
#include "soundent.h"
#include "game_monster_spawning.h"

LINK_ENTITY_TO_CLASS(ent_xen_portal, CXenPortal);

TYPEDESCRIPTION	CXenPortal::m_SaveData[] =
{
	DEFINE_ARRAY(CXenPortal, m_pszMonsterClassname, FIELD_CHARACTER, 64),
	DEFINE_FIELD(CXenPortal, m_iStage, FIELD_INTEGER),
	DEFINE_FIELD(CXenPortal, m_flSpawnTime, FIELD_TIME),
	DEFINE_FIELD(CXenPortal, m_flEndTime, FIELD_TIME)
};

IMPLEMENT_SAVERESTORE(CXenPortal, CBaseEntity);

void CXenPortal::Precache()
{
	PRECACHE_SOUND((char*)GetSound1());
	PRECACHE_SOUND((char*)GetSound2());
	m_iSprite1 = PRECACHE_MODEL((char*)GetSprite1());
	m_iSprite2 = PRECACHE_MODEL((char*)GetSprite2());
	m_iZapBeamSprite = PRECACHE_MODEL("sprites/lgtning.spr");
}

void CXenPortal::Spawn()
{
	Precache();

	m_iStage = 0;

	pev->nextthink = gpGlobals->time + 0.05f;
	SetThink(&CXenPortal::ZapThink);
}

void CXenPortal::ZapThink()
{
	ZapRandomly();
	pev->nextthink = gpGlobals->time;

	switch (m_iStage)
	{
	case 0:
	{
		DoStage0();
		++m_iStage;
	}
	break;
	case 1:
		if (gpGlobals->time >= m_flSpawnTime)
		{
			DoStage1();
			++m_iStage;
		}
		break;
	default:
	case 2:
		if (gpGlobals->time >= m_flEndTime)
		{
			pev->nextthink += 0.1;
			SetThink(&CXenPortal::SUB_Remove);
		}
		break;
	}
}

void CXenPortal::DoStage0()
{
	CSoundEnt::InsertSound(bits_SOUND_COMBAT, pev->origin, 512, 0.5);
	EMIT_SOUND(edict(), CHAN_WEAPON, GetSound1(), 1.0, ATTN_NORM);
	DrawSprite(m_iSprite1);
	DrawSprite(m_iSprite2);
	m_flSpawnTime = gpGlobals->time + .5f;
}

void CXenPortal::DoStage1()
{
	EMIT_SOUND(edict(), CHAN_WEAPON, GetSound2(), 1.0, ATTN_NORM);

	Vector vecAngles(0, RANDOM_FLOAT(-M_PI, +M_PI), 0);
	spawnable_item::g_iRecursionDepth++;
	auto pEntity = Create((char*)m_pszMonsterClassname, pev->origin, vecAngles);
	--spawnable_item::g_iRecursionDepth;
	if (pEntity)
	{
		pEntity->pev->spawnflags |= SF_MONSTER_FALL_TO_GROUND | SF_MONSTER_FADECORPSE;
	}
	m_flEndTime = gpGlobals->time + .5f;
}

void CXenPortal::DrawSprite(int id, float flBrightness, float flSize) const
{
	auto pos = GetEffectsOrigin();
	
	MESSAGE_BEGIN(MSG_PVS, SVC_TEMPENTITY, pev->origin);
	WRITE_BYTE(TE_SPRITE);
	WRITE_COORD(pos.x);	// pos
	WRITE_COORD(pos.y);
	WRITE_COORD(pos.z);
	WRITE_SHORT(id);		// model
	WRITE_BYTE(static_cast<int>(flSize * 10));			// size * 10
	WRITE_BYTE(static_cast<int>(flBrightness * 255));			// brightness
	/*WRITE_BYTE(184);
	WRITE_BYTE(255);
	WRITE_BYTE(214);*/
	MESSAGE_END();
}

void CXenPortal::ZapRandomly()
{
	const auto origin = GetEffectsOrigin();
	for (int iLoops = 0; iLoops < 10; iLoops++)
	{
		auto vecDir = Vector(RANDOM_FLOAT(-1.0, 1.0), RANDOM_FLOAT(-1.0, 1.0), RANDOM_FLOAT(-1.0, 1.0))
			.Normalize();
		TraceResult		tr;
		UTIL_TraceLine(origin, origin + vecDir * m_iZapBeamRadius, ignore_monsters, ENT(pev), &tr);

		if ((tr.vecEndPos - origin).Length() < m_iZapBeamRadius * 0.1)
			continue;

		if (tr.flFraction == 1.0)
			continue;

		DoZap(origin, tr.vecEndPos);
		break;
	}
}

void CXenPortal::DoZap(const Vector & vecSrc, const Vector & vecDest) const
{
	MESSAGE_BEGIN(MSG_BROADCAST, SVC_TEMPENTITY);
	WRITE_BYTE(TE_BEAMPOINTS);
	WRITE_COORD(vecSrc.x);
	WRITE_COORD(vecSrc.y);
	WRITE_COORD(vecSrc.z);
	WRITE_COORD(vecDest.x);
	WRITE_COORD(vecDest.y);
	WRITE_COORD(vecDest.z);
	WRITE_SHORT(m_iZapBeamSprite);
	WRITE_BYTE(0); // framestart
	WRITE_BYTE(10); // framerate
	WRITE_BYTE((int)(m_flZapBeamLife * 10.0)); // life
	WRITE_BYTE(m_iZapBeamWidth);  // width
	WRITE_BYTE(m_iZapBeamNoiseAmplitude);   // noise
	WRITE_BYTE((int)m_vecZapBeamColor.x);   // r, g, b
	WRITE_BYTE((int)m_vecZapBeamColor.y);   // r, g, b
	WRITE_BYTE((int)m_vecZapBeamColor.z);   // r, g, b
	WRITE_BYTE(m_iZapBeamBrightness);	// brightness
	WRITE_BYTE(m_iZapBeamSpeed);		// speed
	MESSAGE_END();
	UTIL_Sparks(vecDest);
}

CXenPortal* CXenPortal::MakePortal(const char* szClassname, const Vector& vecPos)
{
	const auto pEntity = reinterpret_cast<CXenPortal*>(Create("ent_xen_portal", vecPos, { 0, RANDOM_FLOAT(-180, +180), 0 }));
	if (!pEntity)
		return nullptr; 

	pEntity->m_pszMonsterClassname = szClassname;
	return pEntity;
}
