#include	"extdll.h"
#include	"util.h"
#include	"cbase.h"
#include "game_monster_spawning.h"
#include "skill.h"

#define MAX_SPAWNED_MONSTERS 32


class CAlienSpawner : public CPointEntity
{
public:
	void Spawn() override;
	void KeyValue(KeyValueData* pkvd) override;

	virtual int Save(CSave& save);
	virtual int Restore(CRestore& restore);
	static TYPEDESCRIPTION m_SaveData[];

private:
	void EXPORT InitializationThink();
	void EXPORT SpawningThink();
	bool TryFindVacantSlot(int& iFoundSlot);

	EHANDLE m_rgSpawnedMonsters[MAX_SPAWNED_MONSTERS];
	int m_iTotalSpawnedCount;
	int m_iMaxAliveMonsters;
};

TYPEDESCRIPTION CAlienSpawner::m_SaveData[] =
{
	DEFINE_ARRAY(CAlienSpawner, m_rgSpawnedMonsters, FIELD_EHANDLE, MS_MAX_TARGETS),
	DEFINE_FIELD(CAlienSpawner, m_iTotalSpawnedCount, FIELD_INTEGER),
	DEFINE_FIELD(CAlienSpawner, m_iMaxAliveMonsters, FIELD_INTEGER)
};
IMPLEMENT_SAVERESTORE(CAlienSpawner, CPointEntity);

LINK_ENTITY_TO_CLASS(info_alien_spawner, CAlienSpawner);

void CAlienSpawner::Spawn()
{
	pev->solid = SOLID_NOT;
	pev->movetype = MOVETYPE_NONE;

	m_iTotalSpawnedCount = 0;
	m_iMaxAliveMonsters = MAX_SPAWNED_MONSTERS;
	for (auto& m_spawnedMonster : m_rgSpawnedMonsters)
		m_spawnedMonster = nullptr;

	pev->nextthink = gpGlobals->time + 1; // do not immediately start to spawn things
	SetThink(&CAlienSpawner::InitializationThink);
}

void CAlienSpawner::KeyValue(KeyValueData* pkvd)
{
	if (FStrEq(pkvd->szKeyName, "maxalive"))
	{
		m_iMaxAliveMonsters = static_cast<int>(atof(pkvd->szValue));
		pkvd->fHandled = true;
		return;
	}

	CPointEntity::KeyValue(pkvd);
}

float GetRandomSpawningDelay()
{
	float delay = .5f + RANDOM_FLOAT(0, 5);

	switch (g_iSkillLevel)
	{
	default:
	case SKILL_EASY:
		delay *= RANDOM_FLOAT(27, 63); 
		break;
	case SKILL_MEDIUM:
		delay *= RANDOM_FLOAT(9, 21); 
		break;
	case SKILL_HARD:
		delay *= RANDOM_FLOAT(3, 7); 
		break;
	}


	const auto iPlayersCount = max(1, gpGlobals->maxClients - UTIL_GetPlayersCount()+1);
	delay /= sqrtf(RANDOM_LONG(1, iPlayersCount));

	if (gpGlobals->maxClients > 1)
		delay /= 2.f;
	else
		delay *= 2.f;
	
	return max(.25f, delay);
}


void CAlienSpawner::InitializationThink()
{
	auto delay = GetRandomSpawningDelay();
	ALERT(at_console, "First alien spawn in %.2f seconds\n", delay);
	pev->nextthink = gpGlobals->time + delay;
	SetThink(&CAlienSpawner::SpawningThink);
}

void CAlienSpawner::SpawningThink()
{
	int iSlot;
	if (TryFindVacantSlot(iSlot))
	{
		SpawnCategory category;
		if (gpGlobals->maxClients > 1)
		{
			auto rand = RANDOM_FLOAT(0, 1);
			if (rand < 0.66f)
				category = SpawnCategory::Alien;
			else if (rand < 0.90f)
				category = SpawnCategory::HumanMilitary;
			else
				category = SpawnCategory::BlackOps;
		}
		else
		{
			category = SpawnCategory::Alien;
		}
		m_rgSpawnedMonsters[iSlot] = spawnable_item::SpawnOne(category, pev->origin);
	}

	auto delay = GetRandomSpawningDelay();
	ALERT(at_console, "Next alien spawn in %.2f seconds\n", delay);
	pev->nextthink = gpGlobals->time + delay;
}

bool CAlienSpawner::TryFindVacantSlot(int& iFoundSlot)
{
	iFoundSlot = -1;
	int iMonstersAlive = 0;
	for (int iSlot = 0; iSlot < MAX_SPAWNED_MONSTERS; ++iSlot)
	{
		auto& hMonster = m_rgSpawnedMonsters[iSlot];
		auto pEntity = hMonster ? (CBaseEntity*)hMonster : nullptr;
		const auto isAlive = pEntity ? pEntity->IsAlive() : false;
		if (isAlive)
		{
			++iMonstersAlive;
			if (iMonstersAlive >= m_iMaxAliveMonsters)
				return false;
		}
		else
		{
			if (pEntity) 
				pEntity->SUB_StartFadeOut();
			m_rgSpawnedMonsters[iSlot] = nullptr;
			if (iFoundSlot < 0)
			{
				iFoundSlot = iSlot;
				if (iMonstersAlive + (MAX_SPAWNED_MONSTERS - iSlot - 1) <= m_iMaxAliveMonsters)
					return true;
			}
		}
	}
	return iFoundSlot >= 0;	
}
