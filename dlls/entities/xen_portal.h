﻿#pragma once

class CXenPortal : public CBaseEntity
{

public:
	void Precache() override;
	void Spawn() override;
	static CXenPortal* MakePortal(const char* szClassname, const Vector& vecPos);

	int		Save(CSave& save);
	int		Restore(CRestore& restore);
	static	TYPEDESCRIPTION m_SaveData[];
protected:
	virtual const char* GetSound1() const { return "debris/beamstart2.wav"; }
	virtual const char* GetSound2() const { return "debris/beamstart7.wav"; }
	virtual const char* GetSprite1() const { return "sprites/Fexplo1.spr"; }
	virtual const char* GetSprite2() const { return "sprites/XFlare1.spr"; }
private:
	void EXPORT ZapThink();
	void DoStage0();
	void DoStage1();

	void DrawSprite(int id, float flBrightness = 1, float flSize = 1) const;
	void ZapRandomly();
	void DoZap(const Vector& vecSrc, const Vector& vecDest) const;

	Vector GetEffectsOrigin() const { return pev->origin + Vector(0, 0, 64); }
	
	int m_iSprite1;
	int m_iSprite2;
	int m_iZapBeamSprite;
	
	const int m_iZapBeamRadius = 200;
	const float m_flZapBeamLife = 0.5f;
	const int m_iZapBeamWidth = 18;
	const Vector m_vecZapBeamColor = { 0, 255, 0 };
	const int m_iZapBeamNoiseAmplitude = 65;
	const int m_iZapBeamBrightness = 150;
	const int m_iZapBeamSpeed = 35;

	float m_flSpawnTime;
	float m_flEndTime;
	int m_iStage = 0;
	const char* m_pszMonsterClassname = nullptr;
private:
};